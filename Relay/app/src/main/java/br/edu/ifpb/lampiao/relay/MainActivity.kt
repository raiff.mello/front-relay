package br.edu.ifpb.lampiao.relay
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.App.Perfil
import br.edu.ifpb.lampiao.relay.App.Alarm
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val tabela_aplicacao = db.collection("schedule").document(Id_usuarioAtual.toString())

    var calendarAlarm: Calendar = Calendar.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportActionBar?.hide()

        textProxAlarm()




        btn_alarm_register.setOnClickListener {
            goToAlarm()
        }

        btn_historic.setOnClickListener {
            goToHistoric()
        }

        btn_apply.setOnClickListener {
            goToApply()
        }

        btn_profile.setOnClickListener {
            goToProfile()
        }

        bt_refresh.setOnClickListener {
            textProxAlarm()
        }

    }

    private fun goToAlarm() {
        val intent = Intent(this, Alarm::class.java)
        startActivity(intent)
    }

    private fun goToHistoric() {
        val intent = Intent(this, GeneralHistoricActivity::class.java)
        startActivity(intent)
    }

    private fun goToProfile(){
        val intent = Intent(this, Perfil::class.java)
        startActivity(intent)
    }

    private fun goToApply(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
    }

    private fun checkDay(dia: Int, dias: ArrayList<Int>): Boolean {
        var dt = false
        for (n:Int in dias) {
            if (n == dia) {
                dt = true
            }
        }
        return dt
    }

    fun textProxAlarm(){

        tabela_aplicacao.get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val userData = it?.result?.data
                    var horas = userData?.get("horas_aplicacao")
                    var minutos = userData?.get("minutos_aplicacao")

                    if ((horas!=null && minutos!=null)) {

                        var diasBd = userData?.get("dias_semanais_aplication") as ArrayList<Int>
                        horas = horas.toString()
                        minutos = minutos.toString()

                        calendarAlarm.apply {
                            calendarAlarm.timeInMillis = System.currentTimeMillis()
                            set(Calendar.HOUR_OF_DAY, horas.toInt())
                            set(Calendar.MINUTE, minutos.toInt())
                            set(Calendar.SECOND, 0)
                            set(Calendar.MILLISECOND, 0)
                        }

                        while ((!(checkDay(calendarAlarm.get(Calendar.DAY_OF_WEEK), diasBd))) || (System.currentTimeMillis() > calendarAlarm.timeInMillis)) {
                            calendarAlarm.set(Calendar.DAY_OF_MONTH, calendarAlarm.get(Calendar.DAY_OF_MONTH) + 1)
                        }

                        text_horario.text = "$horas:$minutos"
                        var day = calendarAlarm.get(Calendar.DAY_OF_MONTH)
                        var month = calendarAlarm.get(Calendar.MONTH) + 1
                        var year = calendarAlarm.get(Calendar.YEAR)

                        text_dia_e_data.text = "$day/$month/$year"
                    }
                    else{
                        text_dia_e_data.text = "Sem Alarmes"
                        text_horario.text = "00:00"
                    }
                }
            }
    }
}
