package br.edu.ifpb.lampiao.relay.RecyclerView.Model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.BodyData
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields.*

class BodyViewModel : ViewModel() {
    private val _selectedBodyPart = MutableLiveData<BodyData>()
    val selectedBodyPart: LiveData<BodyData> = _selectedBodyPart

    fun setSelectedBodyPart(bodyPart: Int) {
        _selectedBodyPart.value = bodyParts[bodyPart]
    }

    fun getSelectedBodyPart(): BodyData? {
        return _selectedBodyPart.value
    }

    val bodyParts: Array<BodyData>
        get() {
            val list = arrayOf(
                BodyData(None, "", 0, 0),
                BodyData(Belly,"Barriga", R.drawable.belly_zones, R.id.cb_belly),
                BodyData(RightArm, "Braço direito", R.drawable.right_arm_zones, R.id.cb_right_arm),
                BodyData(LeftArm, "Braço esquerdo", R.drawable.left_arm_zones, R.id.cb_left_arm),
                BodyData(RightLeg, "Perna direita", R.drawable.right_leg_zones, R.id.cb_right_leg),
                BodyData(LeftLeg, "Perna esquerda", R.drawable.left_leg_zones, R.id.cb_left_leg),
                BodyData(RightSideHip, "Quadril direito", R.drawable.right_side_hip_zones, R.id.cb_right_side_hip),
                BodyData(LeftSideHip, "Quadril esquerdo", R.drawable.left_side_hip_zones, R.id.cb_left_side_hip),
                BodyData(None, "", 0, 0)
            )
            return list
        }
}