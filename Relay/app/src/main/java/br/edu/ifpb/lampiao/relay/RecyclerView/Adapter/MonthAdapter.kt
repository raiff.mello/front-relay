package br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpb.lampiao.relay.R
import kotlinx.android.synthetic.main.month_item.view.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.TextStyle
import java.util.*

class MonthAdapter(private val items : List<LocalDate>, private val context: Context?,
                   private val monthClickCallback: ((LocalDate) -> Unit)?)
    : RecyclerView.Adapter<MonthViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthViewHolder {
        return MonthViewHolder(LayoutInflater.from(context).inflate(R.layout.month_item, parent, false))
    }

    override fun onBindViewHolder(holderMonth: MonthViewHolder, position: Int) {
        if (position in 1 until (itemCount - 1)) {
            val month = items[position].month.getDisplayName(TextStyle.FULL, Locale("pt", "BR")).toUpperCase()
            holderMonth.monthLabel.text = month
            holderMonth.monthLabel.contentDescription = month
            holderMonth.itemView.setOnClickListener {
                monthClickCallback?.invoke(items[position])
            }
        } else {
            holderMonth.monthLabel.text = ""
            holderMonth.monthLabel.contentDescription = ""
            holderMonth.itemView.setOnClickListener {}
        }
    }
}

class MonthViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val monthLabel: TextView = view.month_label
}
