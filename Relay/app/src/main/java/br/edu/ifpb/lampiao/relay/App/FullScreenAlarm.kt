package br.edu.ifpb.lampiao.relay.App

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.Fragments.NotificationAlarm
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import kotlinx.android.synthetic.main.activity_full_screen_alarm.*

class FullScreenAlarm : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_alarm)
        supportActionBar!!.hide()


        bt_cancelAlarm.setOnClickListener {
            val intentService = Intent(applicationContext, NotificationAlarm::class.java)
            applicationContext.stopService(intentService)
            finish()
        }
    }
    private fun openMainActivity() {
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        openMainActivity()

    }

}