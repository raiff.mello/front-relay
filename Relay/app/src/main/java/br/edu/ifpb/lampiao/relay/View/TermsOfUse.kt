package br.edu.ifpb.lampiao.relay.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.R
import kotlinx.android.synthetic.main.activity_terms_of_use.*

class TermsOfUse : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_of_use)

        supportActionBar?.hide()

        pdfv.fromAsset("output.pdf").load()
    }
}