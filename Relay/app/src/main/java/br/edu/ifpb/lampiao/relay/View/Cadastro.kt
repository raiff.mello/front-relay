package br.edu.ifpb.lampiao.relay.View

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpb.lampiao.relay.Model.DadosUser
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.HistoricDB
import com.github.rtoshiro.util.format.SimpleMaskFormatter
import com.github.rtoshiro.util.format.text.MaskTextWatcher
import com.google.common.io.Resources.copy
import com.google.common.io.Resources.getResource
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_cadastro.*
import java.io.File
import java.lang.reflect.Method
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit


class Cadastro : AppCompatActivity() {
    private val db = Firebase.firestore

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)
        DateMask()
        supportActionBar!!.hide()

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val method: Method =
                    StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                method.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        btn_register.setOnClickListener {
            Register()
        }

        terms.setOnClickListener{
            val intent = Intent(this, TermsOfUse::class.java)
            startActivity(intent)
        }


        val adapter1 = ArrayAdapter.createFromResource(this, R.array.generos, R.layout.spinner_layout)
        adapter1.setDropDownViewResource(R.layout.spinner_layout)
        genero_cadastro.adapter = adapter1

        val adapter3 = ArrayAdapter.createFromResource(this, R.array.dosagem, R.layout.spinner_layout)
        adapter3.setDropDownViewResource(R.layout.spinner_layout)
        dosagem_cadastro.adapter = adapter3

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun Register() {
        val email = email_cadastro.text.toString()
        val pass = senha_cadastro.text.toString()
        val name = nome_cadastro.text.toString()
        val birthdate = birth_date_register.text.toString()
        val gender = genero_cadastro.selectedItem.toString()
        val dosage = dosagem_cadastro.selectedItem.toString()
        val doctor = email_doutor.text.toString()
        val terms = checkbox

        when{
            email == "" || pass == "" || name == "" || birthdate == "" || doctor == "" -> error_message_register.text = "Preencha os campos para cadastrar!"
            pass == name || pass == email || email == name -> error_message_register.text = "Os campos de senha, email e nome não podem ser iguais!"
            name.contains("[0-9!\\\"#\$%&'()*+,-./:;\\\\\\\\<=>?@\\\\[\\\\]^_`{|}~]".toRegex()) -> error_message_register.text = "O nome não deve conter símbolos ou números!"
            !verifyDateField(birthdate) -> error_message_register.text = "A data de nascimento indicada é inválida!"
            doctor == email -> error_message_register.text = "O e-mail do médico não pode ser igual ao e-mail de cadastro do usuário!"
            gender == "Selecione seu gênero" -> error_message_register.text = "Selecione o seu gênero para cadastrar!"
            dosage == "Selecione sua dosagem" -> error_message_register.text = "Selecione a dosagem para cadastrar"
            !terms.isChecked -> error_message_register.text = "Por favor, verifique os Termos de Uso!"
            else -> {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, pass)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val id = it.result?.user?.uid
                                val user = DadosUser(email, pass, name, birthdate, gender, dosage, doctor)
                                db.collection("usuarios").document(id.toString())
                                        .set(user)
                                        .addOnSuccessListener {
                                            HistoricDB(id.toString()).addHistoric()
                                            FirebaseAuth.getInstance().signOut()
                                            goToLogin()
                                        }
                                error_message_register.text = "Cadastro concluído com com sucesso"


                            }
                        }.addOnFailureListener {
                            when (it) {
                                is FirebaseAuthWeakPasswordException -> error_message_register.text = "Sua senha precisa ter no mínimo 6 caracteres!"
                                is FirebaseAuthInvalidCredentialsException -> error_message_register.text = "Insira um e-mail válido!"
                                is FirebaseAuthUserCollisionException -> error_message_register.text = "Esse usuário já existe!"
                                is FirebaseNetworkException -> error_message_register.text = "Conecte-se à Internet!"
                                else -> error_message_register.text = "Erro ao cadastrar!"
                            }
                        }
            }
        }
    }

    private fun goToLogin() {
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
        finish()
    }

    private fun DateMask() {
        val data = birth_date_register
        val smf = SimpleMaskFormatter("NN/NN/NNNN")
        val mtw = MaskTextWatcher(data, smf)
        data.addTextChangedListener(mtw)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun verifyDateField(birthdate: String): Boolean {

        var result = false

        val day = birthdate.substring(0, 2)
        val month = birthdate.substring(3, 5)
        val year = birthdate.substring(6)

        if (verifyDate(birthdate)) {
            val registeddate = LocalDate.of(year.toInt(), month.toInt(), day.toInt())

            val actualdate = LocalDate.now()
            var diffTime = ChronoUnit.DAYS.between(registeddate, actualdate).toInt()
            var diffYears = diffTime/365

            if(diffYears in 0..100){
                if (registeddate.isAfter(actualdate) || registeddate.isEqual(actualdate)) {
                    result = false
                }
                if (registeddate.isBefore(actualdate)) {
                    result = true
                }
            }else{
                result = false
            }
        } else {
            result = false
        }
        return result
    }
    @SuppressLint("SimpleDateFormat")
    private fun verifyDate(date: String): Boolean{
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        sdf.isLenient = false
        return try{
            sdf.parse(date)
            true
        }catch (e: ParseException){
            false
        }
    }
}