package br.edu.ifpb.lampiao.relay.Util

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpb.lampiao.relay.GeneralHistoricActivity
import br.edu.ifpb.lampiao.relay.Model.ApplicationData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*

/*Class responsible for organizing information about the application and responsible for saving
the information collected in the database. It requires that information be given to the area
where the drug was applied.*/

class SaveApplicationDB(siteApplication: String) : AppCompatActivity(){

    var dataBase: FirebaseFirestore? = null
    private val siteAppli = siteApplication
    private val usuarioAtual = Firebase.auth.currentUser


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        dataBase = FirebaseFirestore.getInstance()
    }

    /*---------------------------------------Functions--------------------------------------------*/


    // getIdAppli(): function generates from firebase the ID for each drug application in the app.
    private fun getIdAppli(): String{
        val idAppli: String = dataBase!!.collection("usuarios").document(getIdUser())
                .collection("user_application").document().id
        return idAppli
    }

    // getIdUser(): function that collects the connected user's ID.
    private fun getIdUser(): String {
        val id = usuarioAtual?.uid
        return id.toString()
    }

    // getDate(): function that collects the device's current date, with day, month, year and current time.
    private fun getDate(): String{
        val date = Calendar.getInstance().time
        val dateTimeForm = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault() )
        return dateTimeForm.format(date)
    }

    /* getDate(): function that saves information about drug application in the database. It also
    centralizes the information to be saved. */
    fun saveDataDB(){
        dataBase = FirebaseFirestore.getInstance()
        val document = dataBase!!.collection("usuarios").document(getIdUser())

        document.get().addOnCompleteListener {
            if(it.isSuccessful){
                val userData = it.result?.data
                dataBase = FirebaseFirestore.getInstance()

                // collected the application collection reference
                val reference = dataBase!!.collection("usuarios").document(getIdUser())
                        .collection("user_application")

                val id_appli = getIdAppli()                                 // id of the new application
                val dosage_medicine = userData?.get("dosagem").toString()   // the dosage used

                // Creating an applicationData object with the information to be saved.
                val applicationData = ApplicationData( "Copraxone" , siteAppli,
                        dosage_medicine, getDate() )

                // Saving the applicationData object in the corresponding collection.
                reference.document(id_appli).set(applicationData)


                HistoricDB(siteAppli).appliHistoric()


            }
        }

    }

}

