package br.edu.ifpb.lampiao.relay.Util

enum class EnumBodyFields {
    Belly,
    RightArm,
    LeftArm,
    RightLeg,
    LeftLeg,
    RightSideHip,
    LeftSideHip,
    None
}