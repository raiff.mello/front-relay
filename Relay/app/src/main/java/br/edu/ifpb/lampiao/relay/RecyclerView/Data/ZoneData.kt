package br.edu.ifpb.lampiao.relay.RecyclerView.Data

data class ZoneData(
    val nameZone: String,
    val dateZone: String,
    val progressZone: Int
)