package br.edu.ifpb.lampiao.relay.RecyclerView.Data

import android.widget.CheckBox
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields

data class BodyData(
    val bodyPartEnum: EnumBodyFields,
    val bodyPartName: String,
    val bodyPartImage: Int,
    val bodyPartChecked: Int
)