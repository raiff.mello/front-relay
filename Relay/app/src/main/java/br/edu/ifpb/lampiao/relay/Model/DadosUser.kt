package br.edu.ifpb.lampiao.relay.Model

data class DadosUser(
    val Email:String? = null,
    val Senha:String? = null,
    val Nome:String? = null,
    val data_nascimento:String? = null,
    val genero:String? = null,
    val dosagem:String? = null,
    val email_medico:String? = null
)