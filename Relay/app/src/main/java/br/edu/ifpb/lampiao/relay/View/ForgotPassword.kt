package br.edu.ifpb.lampiao.relay.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthEmailException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPassword : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        supportActionBar?.hide()

        btn_submit_forgot_password.setOnClickListener {
            resetPassword(email_field_forgot_password.text.toString())
        }

        btn_cancel_forgot_password.setOnClickListener {
            returnLogin()
        }
    }

    private fun resetPassword(email: String){
        if (email.isNullOrEmpty()){
            error_message_forgot_password.text = "Por favor, insira seu email para redefinir sua senha!"
        }else{
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val user = FirebaseAuth.getInstance().currentUser
                            error_message_forgot_password.text = "Um email de redefinição de senha foi enviado para o seu email. Por favor, verifique!"
                        }else{
                            error_message_forgot_password.text = "Não foi possível enviar o email para este endereço!"
                        }
                    }.addOnFailureListener {
                        when(it){
                            is FirebaseAuthInvalidCredentialsException -> error_message_forgot_password.text = "Este e-mail é inválido!"
                        }
                    }
        }
    }

    private fun returnLogin(){
        finish()
    }
}