package br.edu.ifpb.lampiao.relay.Util

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpb.lampiao.relay.Model.ApplicationData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.*

class HistoricDB(siteApplication: String) : AppCompatActivity() {

    var dataBase: FirebaseFirestore? = null
    private val siteAppli = siteApplication
    private val usuarioAtual = Firebase.auth.currentUser

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        dataBase = FirebaseFirestore.getInstance()
    }

    // getIdUser(): function that collects the connected user's ID.
    private fun getIdUser(): String {
        val id = usuarioAtual?.uid
        return id.toString()
    }

    private fun getDate(): String {
        val date = Calendar.getInstance().time
        val dateTimeForm = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        return dateTimeForm.format(date)
    }

    fun addHistoric() {
        dataBase = FirebaseFirestore.getInstance()
        val ref = dataBase!!.collection("usuarios").document(siteAppli)


        val list_not_appli = listOf(
                "1A", "1B", "1C", "1D", "1E", "1F",
                "1G", "1H", "1I", "1J", "2A", "2B",
                "2C", "2D", "2E", "2F", "3A", "3B",
                "3C", "3D", "3E", "3F", "4A", "4B",
                "5A", "5B", "6A", "6B", "7A", "7B"
        ) as MutableList<String>
        val date_appli: MutableList<String> = listOf("") as MutableList<String>
        val site_appli: MutableList<String> = listOf("") as MutableList<String>

        val histori_data = hashMapOf(
                "site_appli" to site_appli,
                "date_appli" to date_appli,
                "list_not_appli" to list_not_appli
        )

        val apli = ApplicationData("ex", "ex", "ex", "ex")

        ref.collection("historic").document("historic").set(histori_data)
        ref.collection("user_application").document("init").set(apli)
    }

    private fun updateHistoric(data: DocumentSnapshot?) {
        dataBase = FirebaseFirestore.getInstance()
        val reference = dataBase!!.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        val historic_data = data?.data
        var site_a: MutableList<String> = historic_data?.get("site_appli") as MutableList<String>
        var date_a: MutableList<String> = historic_data?.get("date_appli") as MutableList<String>
        var list_na = historic_data?.get("list_not_appli") as MutableList<String>

        site_a.add(siteAppli)
        date_a.add(getDate())
        list_na.remove(siteAppli)

        reference.update("site_appli", site_a)
        reference.update("date_appli", date_a)
        reference.update("list_not_appli", list_na)

        verifListNoApply(list_na, site_a, date_a, reference)               // verify list not appli is empty

    }

    private fun verifListNoApply(site_not: MutableList<String>, site_appli: MutableList<String>,
                                 date: MutableList<String>, ref: DocumentReference) {

        if (site_not.isEmpty()){
            site_not.add(site_appli[1])
            site_appli.removeAt(1)
            date.removeAt(1)
            ref.update("list_not_appli", site_not)
            ref.update("site_appli", site_appli)
            ref.update("date_appli", date)
        }
    }

    //historic_aplication
    fun appliHistoric() {
        dataBase = FirebaseFirestore.getInstance()
        val reference = dataBase!!.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.get().addOnCompleteListener {
            if (it.isSuccessful) {
                updateHistoric(it.result)
            }
        }
    }

}