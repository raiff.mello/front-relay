package br.edu.ifpb.lampiao.relay.Util

import android.content.Context
import android.widget.Toast
import br.edu.ifpb.lampiao.relay.Fragments.SalveDataAlarm
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class scheduleDB {
    var context: Context?=null
    constructor(context: Context){
        this.context=context
    }

    val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val tabela_aplicacao = db.collection("schedule").document(Id_usuarioAtual.toString())


    fun decrementAplication(){
        tabela_aplicacao.get().addOnCompleteListener {
            if (it.isSuccessful) {
                val userData = it?.result?.data
                var quantidade = userData?.get("quantidade")
                if(quantidade!=null){
                    quantidade = quantidade.toString().toInt()
                    if(quantidade >0) {
                        quantidade -=1
                        tabela_aplicacao.update("quantidade", quantidade)
                    }

                    if(quantidade==0){
                        val saveData = SalveDataAlarm(context!!)
                        saveData.cancelAlarm()
                        Toast.makeText(context, "todas as aplicações foram aplicadas", Toast.LENGTH_SHORT).show()

                    }
                }
            }
        }.addOnFailureListener {
            Toast.makeText(context, "Programe um Alarme", Toast.LENGTH_SHORT).show()

        }
            }
}