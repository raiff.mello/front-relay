package br.edu.ifpb.lampiao.relay.App

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.Aplicacao.*
import br.edu.ifpb.lampiao.relay.R
import kotlinx.android.synthetic.main.activity_aplicacao.*

class Aplicacao : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao)

        supportActionBar?.hide()

        btn_barriga.setOnClickListener {
            aplicacaoBarriga()
            finish()
        }

        btn_perna_direita.setOnClickListener {
            aplicacaoPernaDireita()
            finish()
        }

        btn_perna_esquerda.setOnClickListener {
            aplicacaoPernaEsquerda()
            finish()
        }

        btn_braco_direito.setOnClickListener {
            aplicacaoBracoDireito()
            finish()
        }

        btn_braco_esquerdo.setOnClickListener {
            aplicacaoBracoEsquerdo()
            finish()
        }

        btn_quadril_direito.setOnClickListener {
            aplicacaoQuadrilLadoDireito()
            finish()
        }

        btn_quadril_esquerdo.setOnClickListener {
            aplicacaoQuadrilLadoEsquerdo()
            finish()
        }

        btn_cancel_application.setOnClickListener {
            finish()
        }
    }

    private fun aplicacaoBarriga(){
        val intent = Intent(this, AplicacaoBarriga::class.java)
        startActivity(intent)
    }

    private fun aplicacaoPernaDireita(){
        val intent = Intent(this, AplicacaoPernaDireita::class.java)
        startActivity(intent)
    }

    private fun aplicacaoPernaEsquerda(){
        val intent = Intent(this, AplicacaoPernaEsquerda::class.java)
        startActivity(intent)
    }

    private fun aplicacaoBracoDireito(){
        val intent = Intent(this, AplicacaoBracoDireito::class.java)
        startActivity(intent)
    }

    private fun aplicacaoBracoEsquerdo(){
        val intent = Intent(this, AplicacaoBracoEsquerdo::class.java)
        startActivity(intent)
    }

    private fun aplicacaoQuadrilLadoDireito(){
        val intent = Intent(this, AplicacaoQuadrilLadoDireito::class.java)
        startActivity(intent)
    }

    private fun aplicacaoQuadrilLadoEsquerdo(){
        val intent = Intent(this, AplicacaoQuadrilLadoEsquerdo::class.java)
        startActivity(intent)
    }

}