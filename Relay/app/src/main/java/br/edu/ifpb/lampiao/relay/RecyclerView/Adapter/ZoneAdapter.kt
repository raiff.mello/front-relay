package br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.ZoneData
import kotlinx.android.synthetic.main.zone_item.view.*

class ZoneAdapter(private var items: Array<ZoneData>?, private val context: Context?)
    : RecyclerView.Adapter<ZoneViewHolder>() {

    fun update(items: Array<ZoneData>?) {
        this.items = items
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items?.size!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ZoneViewHolder {
        return ZoneViewHolder(LayoutInflater.from(context).inflate(R.layout.zone_item, parent, false))
    }

    override fun onBindViewHolder(holderZone: ZoneViewHolder, position: Int) {
        holderZone.zoneLabel.text = items?.get(position)!!.nameZone
        holderZone.zoneDate.text = items!![position].dateZone.toString()
        holderZone.zoneProgress.progress = items!![position].progressZone
    }
}

class ZoneViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val zoneLabel: TextView = view.tv_zone_label
    val zoneDate: TextView = view.tv_zone_date
    val zoneProgress: ProgressBar = view.progress_time
}
