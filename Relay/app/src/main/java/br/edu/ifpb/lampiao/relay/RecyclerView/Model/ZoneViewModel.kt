package br.edu.ifpb.lampiao.relay.RecyclerView.Model


import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.edu.ifpb.lampiao.relay.GeneralHistoricActivity
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.ZoneData
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields.*
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.ktx.Firebase

class ZoneViewModel (): ViewModel() {
    private lateinit var firestore : FirebaseFirestore
    private lateinit var usuarioAtual: FirebaseUser
    private var _zones: MutableLiveData<HashMap<Int, Array<ZoneData>>> = MutableLiveData<HashMap<Int, Array<ZoneData>>>()

    init {
        firestore = FirebaseFirestore.getInstance()
        firestore.firestoreSettings = FirebaseFirestoreSettings.Builder().build()
        usuarioAtual = Firebase.auth.currentUser!!
        listenToZones()
    }

    fun listenToZones() {
        val reference = firestore!!.collection("usuarios").document(usuarioAtual?.uid.toString())
            .collection("historic").document("historic")

        var listDate = mutableListOf<String>()
        var listProgress = mutableListOf<Int>()
        reference.addSnapshotListener { local_data, error ->

            var data_his = local_data?.data

            val dateHis: MutableList<String> = data_his?.get("date_appli") as MutableList<String>
            val siteHis: MutableList<String> = data_his.get("site_appli") as MutableList<String>

            val list_date: MutableList<String> = GetHistoricDB("Julho").completeListDate(dateHis,siteHis)

            listDate.addAll(list_date)

            val list_progress: MutableList<Int> = GetHistoricDB("Julho").returnValuesSite(siteHis)

            listProgress.addAll(list_progress)

            _zones.value = hashMapOf(
                Belly.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(0),
                        listProgress.get(0)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(1),
                        listProgress.get(1)
                    ),
                    ZoneData(
                        "C",
                        listDate.get(2),
                        listProgress.get(2)
                    ),
                    ZoneData(
                        "D",
                        listDate.get(3),
                        listProgress.get(3)
                    ),
                    ZoneData(
                        "E",
                        listDate.get(4),
                        listProgress.get(4)
                    ),
                    ZoneData(
                        "F",
                        listDate.get(5),
                        listProgress.get(5)
                    ),
                    ZoneData(
                        "G",
                        listDate.get(6),
                        listProgress.get(6)
                    ),
                    ZoneData(
                        "H",
                        listDate.get(7),
                        listProgress.get(7)
                    ),
                    ZoneData(
                        "I",
                        listDate.get(8),
                        listProgress.get(8)
                    ),
                    ZoneData(
                        "J",
                        listDate.get(9),
                        listProgress.get(9)
                    )
                ), RightArm.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(24),
                        listProgress.get(24)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(25),
                        listProgress.get(25)
                    )
                ), LeftArm.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(22),
                        listProgress.get(22)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(23),
                        listProgress.get(23)
                    )
                ), RightLeg.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(10),
                        listProgress.get(10)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(11),
                        listProgress.get(11)
                    ),
                    ZoneData(
                        "C",
                        listDate.get(12),
                        listProgress.get(12)
                    ),
                    ZoneData(
                        "D",
                        listDate.get(13),
                        listProgress.get(13)
                    ),
                    ZoneData(
                        "E",
                        listDate.get(14),
                        listProgress.get(14)
                    ),
                    ZoneData(
                        "F",
                        listDate.get(15),
                        listProgress.get(15)
                    )
                ), LeftLeg.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(16),
                        listProgress.get(16)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(17),
                        listProgress.get(17)
                    ),
                    ZoneData(
                        "C",
                        listDate.get(18),
                        listProgress.get(18)
                    ),
                    ZoneData(
                        "D",
                        listDate.get(19),
                        listProgress.get(19)
                    ),
                    ZoneData(
                        "E",
                        listDate.get(20),
                        listProgress.get(20)
                    ),
                    ZoneData(
                        "F",
                        listDate.get(21),
                        listProgress.get(21)
                    )
                ), RightSideHip.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(28),
                        listProgress.get(28)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(29),
                        listProgress.get(29)
                    )
                ), LeftSideHip.ordinal to arrayOf(
                    ZoneData(
                        "A",
                        listDate.get(26),
                        listProgress.get(26)
                    ),
                    ZoneData(
                        "B",
                        listDate.get(27),
                        listProgress.get(27)
                    )
                )
            )
        }
    }

    internal var zones: HashMap<Int, Array<ZoneData>>?
        get() { return _zones.value }
        set(value) {_zones.value = value}

    internal var liveZones: MutableLiveData<HashMap<Int, Array<ZoneData>>>
        get() { return _zones }
        set(value) {_zones = value}
}