package br.edu.ifpb.lampiao.relay

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.Window
import android.widget.Button
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.view.setPadding
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.jakewharton.threetenabp.AndroidThreeTen
import kotlinx.android.synthetic.main.activity_general_historic.*
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.EmailAttachment
import org.apache.commons.mail.MultiPartEmail
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Method
import java.security.AccessController.getContext
import java.text.SimpleDateFormat
import java.util.*


class GeneralHistoricActivity : AppCompatActivity() {

    var dataBase: FirebaseFirestore? = null
    private val db = Firebase.firestore
    private val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val document = db.collection("usuarios").document(Id_usuarioAtual.toString())
    val reference = document.collection("historic").document("historic")

    private lateinit var monthSelected: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidThreeTen.init(this)
        setContentView(R.layout.activity_general_historic)
        supportActionBar?.hide()

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val method: Method =
                    StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                method.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        setMoch()

        bt_back.setOnClickListener {
            finish()
        }

        bt_belly.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.Belly.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.Belly)
        }

        bt_right_leg.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.RightLeg.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.RightLeg)
        }

        bt_left_leg.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.LeftLeg.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.LeftLeg)
        }

        bt_right_arm.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.RightArm.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.RightArm)
        }

        bt_left_arm.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.LeftArm.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.LeftArm)
        }

        bt_right_side_hip.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.RightSideHip.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.RightSideHip)
        }

        bt_left_side_hip.setOnClickListener {
            Toast.makeText(this, EnumBodyFields.LeftSideHip.toString(), Toast.LENGTH_SHORT).show()
            startDetailedHistoricActivity(EnumBodyFields.LeftSideHip)
        }

        btn_share_PDF.setOnClickListener {
            showShareDialog()
        }
    }

    private fun setMoch() {
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase!!.collection("usuarios").document(getIdUser())
            .collection("historic").document("historic")

        tr_table_historic1.removeAllViews()
        tr_table_historic2.removeAllViews()
        tr_table_historic3.removeAllViews()

        reference.addSnapshotListener { local_data, error ->
            var data_his = local_data?.data

            val dateHis: MutableList<String> = data_his?.get("date_appli") as MutableList<String>
            val siteHis: MutableList<String> = data_his.get("site_appli") as MutableList<String>

            val list_time: MutableList<String> =
                GetHistoricDB(monthSelected).completeListTime(dateHis)
            val list_site: MutableList<String> =
                GetHistoricDB(monthSelected).completeListSite(dateHis, siteHis)

            mockDays()
            mockAreas(list_site)
            mockTimes(list_time)
        }
    }


    fun startDetailedHistoricActivity(bodyPart: EnumBodyFields) {
        val intent = Intent(this, DetailedHistoricActivity::class.java)
        intent.putExtra("BODY_PART", bodyPart.ordinal)
        startActivity(intent)
    }

    fun mockDays() {
        val tableRow1 = tr_table_historic1
        for (i in 1..30) {
            addColumn(tableRow1, i.toString(), R.color.gray)
        }
    }

    fun mockAreas(area: MutableList<String>) {
        val tableRow2 = tr_table_historic2
        for (i in area) {
            //for(j in arrayOf('a', 'b', 'c'))
            addColumn(tableRow2, i, R.color.middle_green)
        }
    }

    fun mockTimes(time: MutableList<String>) {
        val tableRow3 = tr_table_historic3
        for (i in time) {
            addColumn(tableRow3, i, R.color.teal_200)
        }
    }

    fun addColumn(tableRow: TableRow, title: String, color: Int) {
        val tv_column = TextView(this)
        tv_column.setText(title)
        tv_column.setTextColor(getColor(color))
        tv_column.setBackgroundResource(R.color.light_green)
        tv_column.setPadding((5 * resources.displayMetrics.density).toInt())
        tv_column.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
        tv_column.gravity = Gravity.CENTER
        tableRow.addView(
            tv_column,
            TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1f
            )
        )
    }

    fun getIdUser(): String {
        val id = usuarioAtual?.uid
        return id.toString()
    }

    fun setMonthSelectd(month: String?) {
        monthSelected = month!!
        Toast.makeText(this, month, Toast.LENGTH_SHORT).show()
        setMoch()
    }


    private fun showShareDialog() {
        val dialog = Dialog(this)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)

        dialog.setContentView(R.layout.dialog_layout)

        val savePDF = dialog.findViewById<Button>(R.id.savePDF)
        val sendPDF = dialog.findViewById<Button>(R.id.sendPDF)
        val sharePDF = dialog.findViewById<Button>(R.id.sharePDF)

        savePDF.setOnClickListener {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permission = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    requestPermissions(permission, 1)
                } else {
                    document.get().addOnCompleteListener {task->
                        if (task.isSuccessful) {
                            reference.get().addOnCompleteListener {
                                savePDF(task.result, it.result)
                                openPDF()
                            }
                        } else {
                            Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                    dialog.dismiss()
                }
            } else {
                document.get().addOnCompleteListener {task->
                    if (task.isSuccessful) {
                        reference.get().addOnCompleteListener {
                            savePDF(task.result, it.result)
                            openPDF()
                        }
                    } else {
                        Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
                dialog.dismiss()
            }
        }

        sendPDF.setOnClickListener {
           sendPDFToDoctor()
        }

        sharePDF.setOnClickListener {
            sharePDF()
        }

        dialog.show()
    }

    private fun savePDF(user: DocumentSnapshot?, data: DocumentSnapshot?) {

        if (user != null && user.exists()) {
            val dadosDoUser = user.data

            val name = dadosDoUser?.get("nome").toString()
            val email = dadosDoUser?.get("email").toString()
            val birthday = dadosDoUser?.get("data_nascimento").toString()
            val dosage = dadosDoUser?.get("dosagem").toString()
            val doctor = dadosDoUser?.get("email_medico").toString()
            val gender = dadosDoUser?.get("genero").toString()

            var siteHis: MutableList<String> = data?.data?.get("site_appli") as MutableList<String>
            var dateHis: MutableList<String> = data?.data?.get("date_appli") as MutableList<String>

            val pdfFolder = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), ""
            )

            if (!pdfFolder.exists()) {
                pdfFolder.mkdirs()
                Log.i("create", "Diretório de PDF criado!")
            }

            val mydocument = Document()

            val myFileName = "relay_" + SimpleDateFormat("ddMMyyyy_HH", Locale.getDefault())
                .format(System.currentTimeMillis())

            val myFilePath = pdfFolder.toString() + "/" + myFileName + ".pdf"
            val file = File(myFilePath)

            if (file.exists()) {
                file.delete()
            }

            try {
                PdfWriter.getInstance(mydocument, FileOutputStream(myFilePath))
                mydocument.open()

                fillUserInfo(mydocument, name, email, birthday, gender, dosage, doctor)

                fillUserHistoricInfo(mydocument, dateHis, siteHis)

                mydocument.close()

                Toast.makeText(
                    this,
                    "$myFileName.pdf\n foi salvo em \n$myFilePath",
                    Toast.LENGTH_SHORT
                ).show()


            } catch (e: Exception) {
                Toast.makeText(this, "" + e.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun openPDF() {
        val pdfFolder = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), ""
        )
        val myFileName = "relay_" + SimpleDateFormat("ddMMyyyy_HH", Locale.getDefault())
            .format(System.currentTimeMillis())
        val myFilePath = pdfFolder.toString() + "/" + myFileName + ".pdf"
        val file = File(myFilePath)

        val intent = Intent(Intent.ACTION_VIEW)
        val uri = FileProvider.getUriForFile(
            this.applicationContext,
            this.applicationContext.getPackageName().toString() + ".provider",
            file)
        intent.setDataAndType(uri, "application/pdf")
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(intent)
    }

    private fun sendPDFToDoctor() {
        val db = Firebase.firestore
        val usuarioAtual = Firebase.auth.currentUser
        val Id_usuarioAtual = usuarioAtual?.uid
        val tabela_usuario = db.collection("usuarios").document(Id_usuarioAtual.toString())


        val myFileName = "relay_" + SimpleDateFormat("ddMMyyyy_HH", Locale.getDefault())
            .format(System.currentTimeMillis())
        val myFilePath = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "$myFileName.pdf"
        ).toString()

        val file = File(myFilePath)


        val policy: StrictMode.ThreadPolicy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val myEmail = "lampiaorelay@gmail.com"
        val myPassword = "lampiaop1"



        tabela_usuario.get().addOnCompleteListener {
            if (it.isSuccessful) {
                if (!file.exists()) {

                    document.get().addOnCompleteListener {task->
                        if (task.isSuccessful) {
                            reference.get().addOnCompleteListener {
                                savePDF(task.result, it.result)
                            }
                        } else {
                            Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }

                }
            }

            val userData = it?.result?.data
            var email_medic = userData?.get("email_medico")
            var name = userData?.get("nome")

            if (email_medic != null) {
                val email = MultiPartEmail()
                email.hostName = "smtp.gmail.com"
                email.setSmtpPort(465)
                email.setAuthenticator(DefaultAuthenticator(myEmail, myPassword))
                email.setSSLOnConnect(true)
                email.setStartTLSEnabled(true)
                email.setStartTLSRequired(true)

                try {
                    email.setFrom(myEmail)
                    email.setSubject("Histórico de Aplicações - $name")
                    //email.setMsg("pegou")


                    email.addTo(email_medic.toString())
                    val pdfAnexo = EmailAttachment()
                    pdfAnexo.disposition = EmailAttachment.ATTACHMENT
                    pdfAnexo.path = myFilePath
                    pdfAnexo.name = "Histórico.pdf"

                    email.attach(pdfAnexo)

                    email.send()
                    Toast.makeText(
                        this,
                        "Mensagem enviada com Sucesso!!",
                        Toast.LENGTH_SHORT
                    )
                        .show()


                } catch (e: Exception) {
                    e.printStackTrace()
                    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
                    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

                    if (!isConnected){
                        Toast.makeText(this, "Não foi possível concluir a ação. Por favor, verifique ou conecte-se a uma rede de internet!", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "Ocorreu um erro ao concluir esta ação!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }
        
    }

   private fun sharePDF() {
        val myFileName = "relay_" + SimpleDateFormat("ddMMyyyy_HH", Locale.getDefault())
            .format(System.currentTimeMillis())
        val myFilePath = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "$myFileName.pdf"
        ).toString()

        val file = File(myFilePath)
        val fileUri = Uri.fromFile(file)

        if (!file.exists()) {
            document.get().addOnCompleteListener {task->
                if (task.isSuccessful) {
                    reference.get().addOnCompleteListener {
                        savePDF(task.result, it.result)
                    }
                } else {
                    Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
       val sendIntent: Intent = Intent().apply {
           action = Intent.ACTION_SEND
           putExtra(Intent.EXTRA_STREAM, fileUri)
           type = "application/pdf"
       }

       val shareIntent = Intent.createChooser(sendIntent, null)
       startActivity(shareIntent)
   }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    document.get().addOnCompleteListener {task->
                        if (task.isSuccessful) {
                            reference.get().addOnCompleteListener {
                                savePDF(task.result, it.result)
                                openPDF()
                            }
                        } else {
                            Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } else {
                    Toast.makeText(this, "Permissão negada", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun fillUserInfo(mydocument: Document, name: String, email: String, birthday: String, gender: String, dosage: String, doctor: String) {

        mydocument.addAuthor("Equipe Relay")
        mydocument.addCreationDate()

        val FontNormal =
            Font(Font.FontFamily.TIMES_ROMAN, (12).toFloat(), Font.NORMAL, BaseColor.BLACK)
        val FontMediumBold =
            Font(Font.FontFamily.TIMES_ROMAN, (22).toFloat(), Font.BOLD, BaseColor.BLACK)
        val FontBold =
            Font(Font.FontFamily.TIMES_ROMAN, (12).toFloat(), Font.BOLD, BaseColor.BLACK)
        val FontBigBold =
            Font(Font.FontFamily.TIMES_ROMAN, (28).toFloat(), Font.BOLD, BaseColor.BLACK)

        //adicionando primeiros titulos
        val title = Chunk("Histórico de Aplicações", FontBigBold)
        val paragraphTitle = Paragraph(title)
        paragraphTitle.alignment = Element.ALIGN_CENTER
        paragraphTitle.spacingAfter = 20F
        mydocument.add(paragraphTitle)

        val userTitle = Chunk("Dados do usuário", FontMediumBold)
        val paragraphUserTitle = Paragraph(userTitle)
        paragraphUserTitle.alignment = Element.ALIGN_LEFT
        paragraphUserTitle.spacingAfter = 20F
        mydocument.add(paragraphUserTitle)

        //criando tabela com informações do usuário
        val table = PdfPTable(2)
        table.widthPercentage = 100F
        table.setWidths(floatArrayOf(1F, 2F))

        //variável de manipulação das células da tabela
        var cell: PdfPCell

        val titles =
            arrayOf("Nome", "Email", "Data de nascimento", "Gênero", "Dosagem", "Email do médico")
        val userData = arrayOf(name, email, birthday, gender, dosage, doctor)

        //preenchendo a tabela com os valores passados nas listas
        for (i in titles.indices) {
            cell = PdfPCell(Phrase(titles[i], FontBold))
            cell.setPadding(5F)
            table.addCell(cell)

            cell = PdfPCell(Phrase(userData[i], FontNormal))
            cell.setPadding(5F)
            table.addCell(cell)
        }

        mydocument.add(table)
    }

    private fun fillUserHistoricInfo(mydocument: Document, dateHis: MutableList<String>, siteHis: MutableList<String>) {
        val FontNormal =
            Font(Font.FontFamily.TIMES_ROMAN, (12).toFloat(), Font.NORMAL, BaseColor.BLACK)
        val FontBold =
            Font(Font.FontFamily.TIMES_ROMAN, (12).toFloat(), Font.BOLD, BaseColor.BLACK)
        val FontMediumBold =
            Font(Font.FontFamily.TIMES_ROMAN, (22).toFloat(), Font.BOLD, BaseColor.BLACK)

        //titulo do histórico
        val titleHistoric = Chunk("Histórico de Aplicações", FontMediumBold)
        val paragraphHistoric = Paragraph(titleHistoric)
        paragraphHistoric.alignment = Element.ALIGN_LEFT
        paragraphHistoric.spacingAfter = 20F
        mydocument.add(paragraphHistoric)

        //criando tabela com informações do histórico do usuário
        val table = PdfPTable(3)
        table.widthPercentage = 100F
        table.setWidths(floatArrayOf(1F, 1F, 1F))

        //variável de manipulação das células da tabela
        var cell: PdfPCell

        cell = PdfPCell(Phrase("Data", FontBold))
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.setPadding(5F)
        table.addCell(cell)

        cell = PdfPCell(Phrase("Local de aplicação", FontBold))
        cell.horizontalAlignment = Element.ALIGN_CENTER
        cell.colspan = 2
        cell.setPadding(5F)
        table.addCell(cell)

        var i = 0
       while (i<dateHis.size){
               cell = PdfPCell(Phrase(dateHis[i], FontNormal))
               cell.horizontalAlignment = Element.ALIGN_CENTER
               cell.setPadding(5F)
               table.addCell(cell)

               cell = PdfPCell(Phrase(siteHis[i], FontNormal))
               cell.horizontalAlignment = Element.ALIGN_CENTER
               cell.colspan = 2
               cell.setPadding(5F)
               table.addCell(cell)

           i++
       }
        mydocument.add(table)
    }
}
