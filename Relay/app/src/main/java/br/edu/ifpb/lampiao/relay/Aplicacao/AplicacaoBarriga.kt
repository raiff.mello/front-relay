package br.edu.ifpb.lampiao.relay.Aplicacao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import br.edu.ifpb.lampiao.relay.Util.SaveApplicationDB
import br.edu.ifpb.lampiao.relay.Util.scheduleDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_aplicacao_barriga.*

class AplicacaoBarriga : AppCompatActivity() {
    private var dispoArea = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao_barriga)
        supportActionBar?.hide()

        // Definição da fonte dos botôes de aplicar e cancelar
        val typeface = ResourcesCompat.getFont(this, R.font.mordern_sans)
        bt_aplicar_tela_aplicacao_barriga.typeface = typeface
        bt_cancelar_tela_aplicacao_barriga.typeface = typeface

        consultToAppli()
        var siteApplication = ""

        bt_a1_aa.setOnClickListener {
            if (dispoArea.contains("1A")) {
                setButtonState()
                bt_a1_aa.isChecked = true
                siteApplication = "1A"
            }else {
                bt_a1_aa.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ab.setOnClickListener {
            if (dispoArea.contains("1B")) {
                setButtonState()
                bt_a1_ab.isChecked = true
                siteApplication = "1B"
            }else {
                bt_a1_ab.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ac.setOnClickListener {
            if (dispoArea.contains("1C")) {
                setButtonState()
                bt_a1_ac.isChecked = true
                siteApplication = "1C"
            }else {
                bt_a1_ac.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ad.setOnClickListener {
            if (dispoArea.contains("1D")) {
                setButtonState()
                bt_a1_ad.isChecked = true
                siteApplication = "1D"
            }else {
                bt_a1_ad.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ae.setOnClickListener {
            if (dispoArea.contains("1E")) {
                setButtonState()
                bt_a1_ae.isChecked = true
                siteApplication = "1E"
            }else {
                bt_a1_ae.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_af.setOnClickListener {
            if (dispoArea.contains("1F")) {
                setButtonState()
                bt_a1_af.isChecked = true
                siteApplication = "1F"
            }else {
                bt_a1_af.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ag.setOnClickListener {
            if (dispoArea.contains("1G")) {
                setButtonState()
                bt_a1_ag.isChecked = true
                siteApplication = "1G"
            }else {
                bt_a1_ag.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ah.setOnClickListener {
            if (dispoArea.contains("1H")) {
                setButtonState()
                bt_a1_ah.isChecked = true
                siteApplication = "1H"
            }else {
                bt_a1_ah.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_ai.setOnClickListener {
            if (dispoArea.contains("1I")) {
                setButtonState()
                bt_a1_ai.isChecked = true
                siteApplication = "1I"
            }else {
                bt_a1_ai.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a1_aj.setOnClickListener {
            if (dispoArea.contains("1J")) {
                setButtonState()
                bt_a1_aj.isChecked = true
                siteApplication = "1J"
            }else {
                bt_a1_aj.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_aplicar_tela_aplicacao_barriga.setOnClickListener {
            if (siteApplication != "") {
                SaveApplicationDB(siteApplication).saveDataDB()
                val schedule = scheduleDB(applicationContext)
                schedule.decrementAplication()
                openMenuMain()
            }else{
                Toast.makeText(this, "Você não selecionou uma área", Toast.LENGTH_SHORT).show()
            }
        }

        bt_cancelar_tela_aplicacao_barriga.setOnClickListener {
            openMenuApplication()
        }
    }

    private fun setButtonState(){
        bt_a1_aa.isChecked = false
        bt_a1_ab.isChecked = false
        bt_a1_ac.isChecked = false
        bt_a1_ad.isChecked = false
        bt_a1_ae.isChecked = false
        bt_a1_af.isChecked = false
        bt_a1_ag.isChecked = false
        bt_a1_ah.isChecked = false
        bt_a1_ai.isChecked = false
        bt_a1_aj.isChecked = false
    }

    private fun openMenuMain(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openMenuApplication(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
        finish()
    }


    private fun setPossibleArea(areas: MutableList<String>){
        val area_lis = listOf("1A", "1B", "1C", "1D", "1E", "1F", "1G", "1H", "1I", "1J")

        for (local in area_lis) {
            if (areas.contains(local) == false){
                when(local){
                    "1A" -> bt_a1_aa.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1B" -> bt_a1_ab.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1C" -> bt_a1_ac.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1D" -> bt_a1_ad.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1E" -> bt_a1_ae.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1F" -> bt_a1_af.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1G" -> bt_a1_ag.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1H" -> bt_a1_ah.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1I" -> bt_a1_ai.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "1J" -> bt_a1_aj.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                }
            }
        }
    }

    fun consultToAppli(){
        var dataBase: FirebaseFirestore? = null
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.addSnapshotListener { local_data, error ->
            val data_his = local_data?.data

            val site_noappli: MutableList<String> = data_his?.get("list_not_appli") as MutableList<String>

            override@
            dispoArea = GetHistoricDB("").localAppli("1", site_noappli)

            setPossibleArea(dispoArea)
        }
    }

    fun getIdUser(): String {
        val usuarioAtual = Firebase.auth.currentUser
        val id = usuarioAtual?.uid
        return id.toString()
    }


}


