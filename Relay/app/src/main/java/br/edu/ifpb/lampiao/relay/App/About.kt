package br.edu.ifpb.lampiao.relay.App

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.View.Login
import kotlinx.android.synthetic.main.activity_about.*

class About : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        supportActionBar?.hide()

        bt_back.setOnClickListener {
            returnLogin()

        }

    }

    private fun returnLogin(){
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
        finish()
    }
}