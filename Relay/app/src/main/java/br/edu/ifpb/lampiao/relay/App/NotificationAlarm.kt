package br.edu.ifpb.lampiao.relay.Fragments

import android.app.*
import androidx.core.app.NotificationCompat
import br.edu.ifpb.lampiao.relay.R
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.IBinder
import br.edu.ifpb.lampiao.relay.App.FullScreenAlarm
import br.edu.ifpb.lampiao.relay.Receiver.SampleBootReceiver


class NotificationAlarm : Service() {

    val CHANNEL_ID = "primary_notification_channel"
    private val NOTIFICATION_ID = 0

    var som: Uri? = null
    var tq: Ringtone? = null

    private var notificationWorkManager: NotificationManager? = null

    override fun onCreate() {
        super.onCreate()
        som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        tq = RingtoneManager.getRingtone(this, som)

    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notificationIntent = Intent(this, FullScreenAlarm::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)


        val stopIntent = Intent(this, SampleBootReceiver::class.java).apply {
            action = "stopAlarm"
        }
        val stopPendingIntent: PendingIntent = PendingIntent.getBroadcast(this, 0, stopIntent, 0)

        createChannel()

        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.alarm_clock)
            .setContentTitle("Alarme")
            .setContentText("Hora da Aplicação")
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setFullScreenIntent(pendingIntent, true)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .addAction(R.drawable.alarme_button_shape, "Parar",
                stopPendingIntent)

            .build()



        startForeground(1,notification)

        tq?.play()

        return START_STICKY
    }

    override fun onDestroy() {
        tq?.stop()
        super.onDestroy()
    }

    fun createChannel(){
        notificationWorkManager =
            applicationContext?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager?
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID,
                applicationContext?.getString(R.string.notification_chanel_name),
                NotificationManager.IMPORTANCE_HIGH

            )
            notificationChannel.enableVibration(true)
            notificationWorkManager?.createNotificationChannel(notificationChannel)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


}






