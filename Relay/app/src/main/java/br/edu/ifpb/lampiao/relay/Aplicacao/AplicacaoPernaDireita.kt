package br.edu.ifpb.lampiao.relay.Aplicacao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import br.edu.ifpb.lampiao.relay.Util.SaveApplicationDB
import br.edu.ifpb.lampiao.relay.Util.scheduleDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_aplicacao_barriga.*
import kotlinx.android.synthetic.main.activity_aplicacao_perna_direita.*

class AplicacaoPernaDireita : AppCompatActivity() {
    private var dispoArea = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao_perna_direita)

        supportActionBar?.hide()

        val typeface = ResourcesCompat.getFont(this, R.font.mordern_sans)
        bt_aplicar_tela_aplicacao_perna_direita.typeface = typeface
        bt_cancelar_tela_aplicacao_perna_direita.typeface = typeface

        consultToAppli()
        var siteApplication = ""


        bt_a2_aa.setOnClickListener {
            if (dispoArea.contains("2A")) {
                setButtonState()
                bt_a2_aa.isChecked = true
                siteApplication = "2A"
            }else {
                bt_a2_aa.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a2_ab.setOnClickListener {
            if (dispoArea.contains("2B")) {
                setButtonState()
                bt_a2_ab.isChecked = true
                siteApplication = "2B"
            }else {
                bt_a2_ab.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a2_ac.setOnClickListener {
            if (dispoArea.contains("2C")) {
                setButtonState()
                bt_a2_ac.isChecked = true
                siteApplication = "2C"
            }else {
                bt_a2_ac.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a2_ad.setOnClickListener {
            if (dispoArea.contains("2D")) {
                setButtonState()
                bt_a2_ad.isChecked = true
                siteApplication = "2D"
            }else {
                bt_a2_ad.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a2_ae.setOnClickListener {
            if (dispoArea.contains("2E")) {
                setButtonState()
                bt_a2_ae.isChecked = true
                siteApplication = "2E"
            }else {
                bt_a2_ae.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a2_af.setOnClickListener {
            if (dispoArea.contains("2F")) {
                setButtonState()
                bt_a2_af.isChecked = true
                siteApplication = "2F"
            }else {
                bt_a2_af.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_aplicar_tela_aplicacao_perna_direita.setOnClickListener {
            if (siteApplication != "") {
                SaveApplicationDB(siteApplication).saveDataDB()
                val schedule = scheduleDB(applicationContext)
                schedule.decrementAplication()
                openMenuMain()
            }else{
                Toast.makeText(this, "Você não selecionou uma área", Toast.LENGTH_SHORT).show()
            }
        }

        bt_cancelar_tela_aplicacao_perna_direita.setOnClickListener {
            openMenuApplication()
        }
    }
    private fun setButtonState(){
        bt_a2_aa.isChecked = false
        bt_a2_ab.isChecked = false
        bt_a2_ac.isChecked = false
        bt_a2_ad.isChecked = false
        bt_a2_ae.isChecked = false
        bt_a2_af.isChecked = false
    }
    private fun setPossibleArea(areas: MutableList<String>){
        val area_lis = listOf("2A", "2B", "2C", "2D", "2E", "2F")

        for (local in area_lis) {
            if (areas.contains(local) == false){
                when(local){
                    "2A" -> bt_a2_aa.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "2B" -> bt_a2_ab.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "2C" -> bt_a2_ac.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "2D" -> bt_a2_ad.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "2E" -> bt_a2_ae.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "2F" -> bt_a2_af.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                }
            }
        }
    }

    fun consultToAppli(){
        var dataBase: FirebaseFirestore? = null
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.addSnapshotListener { local_data, error ->
            val data_his = local_data?.data

            val site_noappli: MutableList<String> = data_his?.get("list_not_appli") as MutableList<String>

            override@
            dispoArea = GetHistoricDB("").localAppli("2", site_noappli)

            setPossibleArea(dispoArea)
        }
    }

    fun getIdUser(): String {
        val usuarioAtual = Firebase.auth.currentUser
        val id = usuarioAtual?.uid
        return id.toString()
    }

    private fun openMenuMain(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openMenuApplication(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
        finish()
    }
}