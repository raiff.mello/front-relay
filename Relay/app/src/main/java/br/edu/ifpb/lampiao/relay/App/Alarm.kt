package br.edu.ifpb.lampiao.relay.App


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpb.lampiao.relay.Fragments.SalveDataAlarm
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.Model.DadosAlarm
import br.edu.ifpb.lampiao.relay.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_alarm.*
import kotlin.collections.ArrayList

class Alarm : AppCompatActivity() {
    private val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val tabela_aplicacao = db.collection("schedule").document(Id_usuarioAtual.toString())
    val tabela_usuarios = db.collection("usuarios").document(Id_usuarioAtual.toString())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)

        supportActionBar?.setTitle("Alarme")

        setCheck()


        bt_activate.setOnClickListener {
            cadAlarme()
        }
        bt_cancel.setOnClickListener{
            confirmDel()

        }

        check_quarta.isEnabled = false
        check_quinta.isEnabled = false
        check_sexta.isEnabled = false
        check_sabado.isEnabled = false
        check_domingo.isEnabled = false


        check_segunda.setOnClickListener {
            check_terca.isChecked = false
            check_quinta.isChecked = false
            check_sabado.isChecked = false

            check_quarta.isChecked = !(check_quarta.isChecked)
            check_sexta.isChecked = !(check_sexta.isChecked)


        }
        check_terca.setOnClickListener {
            check_segunda.isChecked = false
            check_quarta.isChecked = false
            check_sexta.isChecked = false

            check_quinta.isChecked = !(check_quinta.isChecked)
            check_sabado.isChecked = !(check_sabado.isChecked)


        }


    }



    private fun cadAlarme(){
        var hora_selecionada = tp_alarm.hour
        var min_selecionada = tp_alarm.minute
        val quantidade =et_quantity.text.toString()
        var dias_semana_aplication:ArrayList<Int> = arrayListOf<Int>()


        if(quantidade.isEmpty()){
            Toast.makeText(this, "Preencha todos os campos", Toast.LENGTH_SHORT).show()
        }else {
            if (check_domingo.isChecked) dias_semana_aplication.add(1)
            if (check_segunda.isChecked) dias_semana_aplication.add(2)
            if (check_terca.isChecked) dias_semana_aplication.add(3)
            if (check_quarta.isChecked) dias_semana_aplication.add(4)
            if (check_quinta.isChecked) dias_semana_aplication.add(5)
            if (check_sexta.isChecked) dias_semana_aplication.add(6)
            if (check_sabado.isChecked) dias_semana_aplication.add(7)

            if (!(dias_semana_aplication.isEmpty())) {
                val dadosAlarme = DadosAlarm(
                    dias_semana_aplication,
                    hora_selecionada,
                    min_selecionada,
                    quantidade.toInt()
                )
                tabela_aplicacao.set(dadosAlarme)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Alarme cadastrado com sucesso", Toast.LENGTH_SHORT)
                            .show()
                        finish()
                    }

                val saveData = SalveDataAlarm(applicationContext)
                saveData.ativarAlarmes()

            }else{
                Toast.makeText(this, "Informe os dias", Toast.LENGTH_SHORT)
                    .show()
            }
        }

    }


    private fun setCheck() {
        tabela_usuarios.get().addOnCompleteListener{
            val userData = it?.result?.data
            val doseUser = userData?.get("dosagem").toString()

            when (doseUser) {
                "20mg" -> {
                    check_segunda.isEnabled = false
                    check_terca.isEnabled = false

                    bt_daily.setBackgroundResource(R.drawable.shape_botao)
                    bt_daily.setHintTextColor(resources.getColor(R.color.white))
                    bt_daily.isSelected = true
                    bt_weekily.isEnabled = false
                    check_segunda.isChecked = true
                    check_terca.isChecked = true
                    check_quarta.isChecked = true
                    check_quinta.isChecked = true
                    check_sexta.isChecked = true
                    check_sabado.isChecked = true
                    check_domingo.isChecked = true

                }
                "40mg" -> {

                    bt_weekily.setBackgroundResource(R.drawable.shape_botao)
                    bt_weekily.setHintTextColor(resources.getColor(R.color.white))
                    bt_daily.isEnabled = false
                    bt_weekily.isSelected = true



                }
            }

        }
    }
    private fun openMainMenu() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun confirmDel(){
        var confirm = AlertDialog.Builder(this)
        confirm.setMessage("Deseja Excluir os Alarmes")
        confirm.setTitle("Excluir Alarme")
        confirm.setPositiveButton("Sim", DialogInterface.OnClickListener { dialog, which ->
            val saveData= SalveDataAlarm(applicationContext)
            saveData.cancelDeleteAlarm()
            finish()
        })
        confirm.setNegativeButton("Não", null)
        confirm.show()
    }



}




