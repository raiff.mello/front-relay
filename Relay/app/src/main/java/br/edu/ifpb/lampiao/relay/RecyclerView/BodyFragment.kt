package br.edu.ifpb.lampiao.relay.RecyclerView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.CheckBox
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter.BodyAdapter
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Model.BodyViewModel
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.BodyData
import br.edu.ifpb.lampiao.relay.RecyclerView.Model.ZoneViewModel
import br.edu.ifpb.lampiao.relay.Util.CustomLinearSnapHelper
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields.*
import kotlinx.android.synthetic.main.activity_detailed_historic.*
import kotlinx.android.synthetic.main.body_item.*
import kotlinx.android.synthetic.main.fragment_body.*


class BodyFragment: Fragment() {
    private lateinit var bodyViewModel: BodyViewModel
    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var navHostFragment: NavHostFragment
    private lateinit var currentFragment: ZoneFragment

    private lateinit var zoneViewModel: ZoneViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_body, container, false)

        layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        bodyViewModel = ViewModelProvider(this).get(BodyViewModel::class.java)


        body_part_list.adapter = BodyAdapter(
            bodyViewModel.bodyParts,
            context) { bodyPart ->
            bodyViewModel.setSelectedBodyPart(bodyViewModel.bodyParts.indexOf(bodyPart))
            scrollToBodyPart(bodyPart)
        }
        body_part_list.layoutManager = layoutManager

        val bodyPartEnum = requireActivity().intent?.getIntExtra("BODY_PART", 1)
        val index = bodyViewModel.bodyParts.first{ it.bodyPartEnum.ordinal == bodyPartEnum}
        val bodyPartIndex = bodyViewModel.bodyParts.indexOf(index)
        initializeSelectedBodyPart(bodyPartIndex)

        zoneViewModel = ViewModelProvider(requireActivity()).get(ZoneViewModel::class.java)
        zoneViewModel.liveZones.observe(requireActivity(), Observer {
            setSelectdBodyPart(bodyViewModel.getSelectedBodyPart()!!)
        })

        decrement_body_party.setOnClickListener {
            bodyViewModel.selectedBodyPart.value?.let { currentBodyPart ->
                val currentIndex = bodyViewModel.bodyParts.indexOf(currentBodyPart)
                if (currentIndex > 1) {
                    val previousCurrentBody = currentIndex - 1
                    requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = false
                    bodyViewModel.setSelectedBodyPart(previousCurrentBody)
                    requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = true
                    scrollToBodyPart(bodyViewModel.getSelectedBodyPart())
                }
            }
        }

        increment_body_part.setOnClickListener {
            bodyViewModel.selectedBodyPart.value?.let { currentBodyPart ->
                val currentIndex = bodyViewModel.bodyParts.indexOf(currentBodyPart)
                if (currentIndex < bodyViewModel.bodyParts.size - 2) {
                    val nextBodyPart = currentIndex + 1
                    requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = false
                    bodyViewModel.setSelectedBodyPart(nextBodyPart)
                    requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = true
                    scrollToBodyPart(bodyViewModel.getSelectedBodyPart())
                }
            }
        }

        val snapHelper = CustomLinearSnapHelper(context)
        snapHelper.attachToRecyclerView(body_part_list)
        body_part_list.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE) {
                    val offset = (body_part_list.width / body_item.width - 1) / 2
                    val position = layoutManager.findFirstCompletelyVisibleItemPosition() + offset
                    if (position in 0 until bodyViewModel.bodyParts.size &&
                        bodyViewModel.bodyParts[position] != bodyViewModel.selectedBodyPart.value) {
                        when (position) {
                            0 -> {
                                bodyViewModel.setSelectedBodyPart(1)
                                scrollToBodyPart(bodyViewModel.bodyParts[1])
                            }
                            bodyViewModel.bodyParts.size - 1 -> {
                                bodyViewModel.setSelectedBodyPart(position - 1)
                                scrollToBodyPart(bodyViewModel.bodyParts[position - 1])
                            }
                            else -> {
                                requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = false
                                bodyViewModel.setSelectedBodyPart(position)
                                requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = true
                                refreshZones(bodyViewModel.getSelectedBodyPart())
                            }
                        }
                    }
                }
            }
        })

        requireActivity().bt_belly.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == Belly})
        }

        requireActivity().bt_right_leg.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == RightLeg})
        }

        requireActivity().bt_left_leg.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == LeftLeg})
        }

        requireActivity().bt_right_arm.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == RightArm})
        }

        requireActivity().bt_left_arm.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == LeftArm})
        }

        requireActivity().bt_right_side_hip.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == RightSideHip})
        }

        requireActivity().bt_left_side_hip.setOnClickListener {
            setSelectdBodyPart(bodyViewModel.bodyParts.first{ it.bodyPartEnum == LeftSideHip})
        }
    }

    private fun initializeSelectedBodyPart(bodyPart: Int) {
        if (bodyViewModel.selectedBodyPart.value == null) {
            bodyViewModel.setSelectedBodyPart(bodyPart)
            scrollToBodyPart(bodyViewModel.bodyParts.get(bodyPart))
            requireActivity().findViewById<CheckBox>(bodyViewModel.bodyParts[bodyPart].bodyPartChecked).isVisible = true
        } else {
            scrollToBodyPart(bodyViewModel.getSelectedBodyPart())
            requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = true
        }
    }

    private fun setSelectdBodyPart(bodyPart: BodyData) {
        requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = false
        bodyViewModel.setSelectedBodyPart(bodyViewModel.bodyParts.indexOf(bodyPart))
        scrollToBodyPart(bodyViewModel.getSelectedBodyPart())
        requireActivity().findViewById<CheckBox>(bodyViewModel.getSelectedBodyPart()?.bodyPartChecked!!).isVisible = true
    }


    private fun scrollToBodyPart(bodyPart: BodyData?) {
        var width = body_part_list.width
        val index = bodyViewModel.bodyParts.indexOf(bodyPart)

        if (width > 0) {
            val monthWidth = body_item.width
            layoutManager.scrollToPositionWithOffset(index, width / 2 - monthWidth / 2)
        } else {
            val vto = body_part_list.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    body_part_list.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    width = body_part_list.width
                    context?.resources?.getDimensionPixelSize(R.dimen.body_item_width)?.let { monthWidth ->
                        layoutManager.scrollToPositionWithOffset(index, width / 2 - monthWidth / 2)
                    }
                }
            })
        }
        refreshZones(bodyViewModel.getSelectedBodyPart())
    }

    fun refreshZones(bodyPart: BodyData?) {
        if(requireActivity().supportFragmentManager.findFragmentById(R.id.zone_fragment) != null) {
            navHostFragment =
                requireActivity().supportFragmentManager.fragments[0] as NavHostFragment
            currentFragment =
                navHostFragment.childFragmentManager?.fragments?.get(0) as ZoneFragment
            currentFragment.setBodyPartSelectd(bodyPart)
        }
    }
}
