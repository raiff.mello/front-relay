package br.edu.ifpb.lampiao.relay.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import br.edu.ifpb.lampiao.relay.App.Perfil
import br.edu.ifpb.lampiao.relay.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_perfil.*
import kotlinx.android.synthetic.main.activity_redefinir_senha.*

class RedefinirSenha : AppCompatActivity() {
    private val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val document = db.collection("usuarios").document(Id_usuarioAtual.toString())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redefinir_senha)

        supportActionBar?.hide()


        bt_alterar_senha.setOnClickListener {
            document.get().addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = it.result

                    if (user != null && user.exists()) {
                        val dadosDoUser = user.data

                        val senhaAtualbanco = dadosDoUser?.get("senha").toString()
                        alterarSenha(senhaAtualbanco)


                    } else {
                        Toast.makeText(this, "Erro ao ler usuário", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT).show()
                }
            }
        }

        bt_cancel_reset_pass.setOnClickListener {
            finish()
        }
    }

    private fun alterarSenha(senhaAtualBanco: String) {

        val senhaAtualDigitada = senha_atual.text.toString()
        val novaSenha = nova_senha.text.toString()
        val confirmaNovaSenha = confirmar_nova_senha.text.toString()

        if (senhaAtualDigitada != "" || novaSenha != "" || confirmaNovaSenha != "") {
            if (novaSenha.length < 6) {
                msg_error_reset_pass.text = "A senha deve ter ao menos 6 caracteres!"
            } else {
                if (senhaAtualBanco == senhaAtualDigitada) {
                    if (novaSenha == confirmaNovaSenha) {
                        if (senhaAtualBanco == novaSenha) {
                            msg_error_reset_pass.text = "A nova senha deve ser diferente da senha atual!"
                        } else {
                            document.update("senha", novaSenha)
                            usuarioAtual!!.updatePassword(novaSenha)
                            msg_error_reset_pass.text = "Senha alterada com sucesso!"
                            finish()
                        }
                    } else {
                        msg_error_reset_pass.text = "Os campos não coincidem!"
                    }
                } else {
                    msg_error_reset_pass.text = "A senha atual está incorreta!"
                }
            }
        }else {
            msg_error_reset_pass.text = "Preencha os campos para alterar a senha!"
        }
    }
}

