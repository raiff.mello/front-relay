package br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.BodyData
import kotlinx.android.synthetic.main.body_item.view.*

class BodyAdapter(private val items: Array<BodyData>,
                  private val context: Context?,
                  private val bodyClickCallback: ((BodyData) -> Unit)?)
    : RecyclerView.Adapter<BodyViewHolder>() {
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BodyViewHolder {
        return BodyViewHolder(LayoutInflater.from(context).inflate(R.layout.body_item, parent, false))
    }

    override fun onBindViewHolder(holderBody: BodyViewHolder, position: Int) {
        if (position in 1 until (itemCount - 1)) {
            val body = items[position]
            holderBody.bodyLabel.text = body.bodyPartName
            holderBody.bodyLabel.contentDescription = body.bodyPartName
            holderBody.bodyPartImage.setImageResource(items[position].bodyPartImage)
            holderBody.itemView.setOnClickListener {
                bodyClickCallback?.invoke(items[position])
            }
        } else {
            holderBody.bodyLabel.text = ""
            holderBody.bodyLabel.contentDescription = ""
            holderBody.itemView.setOnClickListener {}
            holderBody.bodyPartImage.setImageResource(0)
            holderBody.itemView.setOnClickListener {}
        }
    }
}

class BodyViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val bodyLabel: TextView = view.body_label
    val bodyPartImage: ImageView = view.body_part_image
}
