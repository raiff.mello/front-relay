package br.edu.ifpb.lampiao.relay.RecyclerView

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpb.lampiao.relay.GeneralHistoricActivity
import br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter.MonthAdapter
import br.edu.ifpb.lampiao.relay.RecyclerView.Model.MonthViewModel
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.BodyData
import br.edu.ifpb.lampiao.relay.Util.CustomLinearSnapHelper
import kotlinx.android.synthetic.main.fragment_month.*
import kotlinx.android.synthetic.main.month_item.*
import org.threeten.bp.format.TextStyle
import java.time.LocalDate
import java.util.*

class MonthFragment: Fragment() {
    private lateinit var monthViewModel: MonthViewModel

    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_month, container, false)

        layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL

        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        monthViewModel = ViewModelProvider(this).get(MonthViewModel::class.java)

        month_list.adapter = MonthAdapter(monthViewModel.months, context) { date ->
            monthViewModel.setSelectedMonth(date)
            scrollToMonth(date)
        }
        month_list.layoutManager = layoutManager

        initializeSelectedMonth()

        decrement_month.setOnClickListener {
            monthViewModel.selectedMonth.value?.let { currentDate ->
                val date = currentDate.minusMonths(1)
                if (monthViewModel.months.indexOf(date) >= 0) {
                    monthViewModel.setSelectedMonth(date)
                    refreshApplicationTable()
                    scrollToMonth(date)
                }
            }
        }

        increment_month.setOnClickListener {
            monthViewModel.selectedMonth.value?.let { currentDate ->
                val date = currentDate.plusMonths(1)
                if (monthViewModel.months.indexOf(date) >= 0) {
                    monthViewModel.setSelectedMonth(date)
                    refreshApplicationTable()
                    scrollToMonth(date)
                }
            }
        }

        val snapHelper = CustomLinearSnapHelper(context)
        snapHelper.attachToRecyclerView(month_list)
        month_list.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE) {
                    val offset = (month_list.width / month_item.width - 1) / 2
                    val position = layoutManager.findFirstCompletelyVisibleItemPosition() + offset
                    if (position in 0 until monthViewModel.months.size &&
                        monthViewModel.months[position] != monthViewModel.selectedMonth.value) {
                        when (position) {
                            0 -> {
                                monthViewModel.setSelectedMonth(monthViewModel.months[1])
                                scrollToMonth(monthViewModel.months[1])
                            }
                            monthViewModel.months.size - 1 -> {
                                monthViewModel.setSelectedMonth(monthViewModel.months[position - 1])
                                scrollToMonth(monthViewModel.months[position - 1])
                            }
                            else -> {
                                monthViewModel.setSelectedMonth(monthViewModel.months[position])
                                refreshApplicationTable()
                            }
                        }
                    }
                }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initializeSelectedMonth() {
        if (monthViewModel.selectedMonth.value == null) {
            val now = monthViewModel.months[LocalDate.now().monthValue]
            monthViewModel.setSelectedMonth(now)
            refreshApplicationTable()
            scrollToMonth(now)
        }
    }

    private fun scrollToMonth(month: org.threeten.bp.LocalDate) {
        var width = month_list.width

        if (width > 0) {
            val monthWidth = month_item.width
            layoutManager.scrollToPositionWithOffset(monthViewModel.months.indexOf(month), width / 2 - monthWidth / 2)
        } else {
            val vto = month_list.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    month_list.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    width = month_list.width
                    context?.resources?.getDimensionPixelSize(R.dimen.month_item_width)?.let { monthWidth ->
                        layoutManager.scrollToPositionWithOffset(monthViewModel.months.indexOf(month), width / 2 - monthWidth / 2)
                    }
                }
            })
        }
    }

    fun refreshApplicationTable() {
        val activityParent = requireActivity() as GeneralHistoricActivity
        activityParent.setMonthSelectd(
            monthViewModel.getSelectedMonth()?.month?.getDisplayName(
            TextStyle.FULL, Locale("pt", "BR"))
        )
    }
}
