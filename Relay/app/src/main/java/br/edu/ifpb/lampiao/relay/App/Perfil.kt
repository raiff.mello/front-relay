package br.edu.ifpb.lampiao.relay.App

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.get
import androidx.core.view.isVisible
import br.edu.ifpb.lampiao.relay.Fragments.SalveDataAlarm
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.View.Login
import br.edu.ifpb.lampiao.relay.View.RedefinirSenha
import com.github.rtoshiro.util.format.SimpleMaskFormatter
import com.github.rtoshiro.util.format.text.MaskTextWatcher
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_cadastro.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_perfil.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit

class Perfil : AppCompatActivity() {

    private val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val document = db.collection("usuarios").document(Id_usuarioAtual.toString())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)
        supportActionBar?.hide()
        MascaraData()


        val adapter1 = ArrayAdapter.createFromResource(this, R.array.generos, R.layout.spinner_layout)
        adapter1.setDropDownViewResource(R.layout.spinner_layout)
        genero_view.adapter = adapter1


        val adapter3 = ArrayAdapter.createFromResource(this, R.array.dosagem, R.layout.spinner_layout)
        adapter3.setDropDownViewResource(R.layout.spinner_layout)
        dosagem_view.adapter = adapter3

        desativarEdit()

        bt_sair.setOnClickListener {
            irParaLogin()
        }

        bt_editar.setOnClickListener {
            ativarEdit()


        bt_salvar.setOnClickListener {
            editDadosUser()
        }
        }

        alterar_senha.setOnClickListener {
            val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

            if (!isConnected) {
                Toast.makeText(
                    this,
                    "Não é possível acessar este recurso. Por favor, verifique ou conecte-se a uma rede de internet!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val intent = Intent(this, RedefinirSenha::class.java)
                startActivity(intent)
            }
        }


        document.get().addOnCompleteListener {
            if(it.isSuccessful){
                readUserBanco(it.result)

            }else{
                Toast.makeText(this, "Erro ao ler Dados do servidor", Toast.LENGTH_SHORT).show()
            }
        }

    }




    private fun readUserBanco(user:DocumentSnapshot?){
        if (user != null && user.exists()){
            val dadosDoUser = user.data

            val nome = dadosDoUser?.get("nome").toString()
            val email = dadosDoUser?.get("email").toString()
            val senha = dadosDoUser?.get("senha").toString()
            val data_nasc = dadosDoUser?.get("data_nascimento").toString()
            val dosagem = dadosDoUser?.get("dosagem").toString()
            val email_med = dadosDoUser?.get("email_medico").toString()
            val genero = dadosDoUser?.get("genero").toString()

            nome_text.setText(nome)
            email_text.setText(email)
            email_dr.setText(email_med)
            nascimento_text.setText(data_nasc)

            val arrayDosagem = resources.getStringArray(R.array.dosagem)
            val positionDosage = getIndiceSpiner(dosagem,arrayDosagem)
            dosagem_view.setSelection(positionDosage)
            //Captura o array do spiner, busca o indice do elemento escolhido, e seta seu valor

            val arrayGenero = resources.getStringArray(R.array.generos)
            val positionGenero = getIndiceSpiner(genero, arrayGenero)
            genero_view.setSelection(positionGenero)

        }else{
            Toast.makeText(this, "Erro ao ler usuário", Toast.LENGTH_SHORT).show()
        }
    }

    private fun editDadosUser(){
        val edit_email = email_text.text.toString()
        val edit_nome = nome_text.text.toString()
        val edit_datanasc = nascimento_text.text.toString()
        val edit_gen = genero_view.selectedItem.toString()
        val edit_dosagem = dosagem_view.selectedItem.toString()
        val edit_medico = email_dr.text.toString()

        if (edit_medico.isEmpty()||edit_email.isEmpty()|| edit_nome.isEmpty()|| edit_datanasc.isEmpty()|| edit_gen == "Selecione seu gênero" || edit_dosagem == "Selecione sua dosagem") {
            Toast.makeText(this, "ERRO! Todos os campos devem ser preenchidos", Toast.LENGTH_SHORT).show()
        }else if(edit_nome.contains("[0-9!\\\"#\$%&'()*+,-./:;\\\\\\\\<=>?@\\\\[\\\\]^_`{|}~]".toRegex())){
            Toast.makeText(this, "ERRO! O nome não deve conter símbolos ou números!", Toast.LENGTH_SHORT).show()
        }else if (!verifyDateField(edit_datanasc)){
            Toast.makeText(this, "ERRO! A data de nascimento indicada é inválida!", Toast.LENGTH_SHORT).show()
        }
        else {
            usuarioAtual!!.updateEmail(edit_email)
            document.update(
                mapOf(
                    "nome" to edit_nome,
                    "email" to edit_email,
                    "data_nascimento" to edit_datanasc,
                    "dosagem" to edit_dosagem,
                    "email_medico" to edit_medico,
                    "genero" to edit_gen
                )
            )
            desativarEdit()
        }
    }

    private fun desativarEdit(){

        nome_text.setBackgroundResource(R.drawable.campo_desativado)
        nome_text.isEnabled = false

        nascimento_text.setBackgroundResource(R.drawable.campo_desativado)
        nascimento_text.isEnabled = false

        email_dr.setBackgroundResource(R.drawable.campo_desativado)
        email_dr.isEnabled = false

        email_text.setBackgroundResource(R.drawable.campo_desativado)
        email_text.isEnabled = false

        genero_view.setBackgroundResource(R.drawable.campo_desativado)
        genero_view.isEnabled = false

        dosagem_view.setBackgroundResource(R.drawable.campo_desativado)
        dosagem_view.isEnabled = false

        bt_editar.isVisible = true
        bt_salvar.isVisible = false

    }

    private fun ativarEdit(){
        nome_text.setBackgroundResource(R.drawable.shape_campo)
        nome_text.isEnabled = true

        nascimento_text.setBackgroundResource(R.drawable.shape_campo)
        nascimento_text.isEnabled = true

        email_dr.isEnabled = true
        email_dr.setBackgroundResource(R.drawable.shape_campo)

        email_text.isEnabled = true
        email_text.setBackgroundResource(R.drawable.shape_campo)

        genero_view.isEnabled = true
        genero_view.setBackgroundResource(R.drawable.shape_campo)

        dosagem_view.isEnabled = true
        dosagem_view.setBackgroundResource(R.drawable.shape_campo)



        bt_editar.isVisible = false
        bt_salvar.isVisible = true

    }

    private fun getIndiceSpiner(item:String, arr:Array<String>):Int{
        var contador = 0
        for(dose in arr){
            if(dose==item){
                break
            }else{contador++}
        }
        return contador
    }



    private fun irParaLogin(){
        var confirm = AlertDialog.Builder(this)
        confirm.setMessage("Deseja Sair do Aplicativo?")
        confirm.setTitle("Sair")
        confirm.setPositiveButton("Sim", DialogInterface.OnClickListener { dialog, which ->
            val intent = Intent(this, Login::class.java)
            val saveData = SalveDataAlarm(applicationContext)
            saveData.cancelAlarm()
            startActivity(intent)
            FirebaseAuth.getInstance().signOut()
            finish()
        })
        confirm.setNegativeButton("Não", null)
        confirm.show()


    }

    private fun verifyDateField(birthdate: String): Boolean {

        var result = false

        val day = birthdate.substring(0, 2)
        val month = birthdate.substring(3, 5)
        val year = birthdate.substring(6)

        if (verifyDate(birthdate)) {
            val registeddate = LocalDate.of(year.toInt(), month.toInt(), day.toInt())

            val actualdate = LocalDate.now()
            var diffTime = ChronoUnit.DAYS.between(registeddate, actualdate).toInt()
            var diffYears = diffTime/365

            if(diffYears in 0..100){
                if (registeddate.isAfter(actualdate) || registeddate.isEqual(actualdate)) {
                    result = false
                }
                if (registeddate.isBefore(actualdate)) {
                    result = true
                }
            }else{
                result = false
            }
        } else {
            result = false
        }
        return result
    }
    @SuppressLint("SimpleDateFormat")
    private fun verifyDate(date: String): Boolean{
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        sdf.isLenient = false
        return try{
            sdf.parse(date)
            true
        }catch (e: ParseException){
            false
        }
    }


    private fun MascaraData(){
        var data = nascimento_text
        var smf = SimpleMaskFormatter("NN/NN/NNNN")
        var mtw = MaskTextWatcher(data, smf)
        data.addTextChangedListener(mtw)
    }
}