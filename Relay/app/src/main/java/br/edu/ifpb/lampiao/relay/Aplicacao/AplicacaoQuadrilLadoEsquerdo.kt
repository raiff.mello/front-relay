package br.edu.ifpb.lampiao.relay.Aplicacao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import br.edu.ifpb.lampiao.relay.Util.HistoricDB
import br.edu.ifpb.lampiao.relay.Util.SaveApplicationDB
import br.edu.ifpb.lampiao.relay.Util.scheduleDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_aplicacao_barriga.*
import kotlinx.android.synthetic.main.activity_aplicacao_braco_direito.*
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_direito.*
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_esquerdo.*
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_esquerdo.bt_a6_aa
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_esquerdo.bt_a6_ab
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_esquerdo.bt_aplicar_tela_aplicacao_quadril_esquerdo

class AplicacaoQuadrilLadoEsquerdo : AppCompatActivity() {
    private var dispoArea = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao_quadril_lado_esquerdo)

        supportActionBar?.hide()

        val typeface = ResourcesCompat.getFont(this, R.font.mordern_sans)
        bt_aplicar_tela_aplicacao_quadril_esquerdo.typeface = typeface
        bt_cancelar_tela_aplicacao_quadril_esquerdo.typeface = typeface

        consultToAppli()
        var siteApplication = ""

        bt_a6_aa.setOnClickListener {
            if (dispoArea.contains("6A")) {
                bt_a6_aa.isChecked = true
                bt_a6_ab.isChecked = false
                siteApplication = "6A"
            }else {
                bt_a6_aa.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }

        }

        bt_a6_ab.setOnClickListener {
            if (dispoArea.contains("6B")) {
                bt_a6_aa.isChecked = false
                bt_a6_ab.isChecked = true
                siteApplication = "6B"
            }else {
                bt_a6_ab.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }

        }

        bt_aplicar_tela_aplicacao_quadril_esquerdo.setOnClickListener {
            if (siteApplication != "") {
                SaveApplicationDB(siteApplication).saveDataDB()
                val schedule = scheduleDB(applicationContext)
                schedule.decrementAplication()
                openMenuMain()
            }else{
                Toast.makeText(this, "Você não selecionou uma área", Toast.LENGTH_SHORT).show()
            }
        }

        bt_cancelar_tela_aplicacao_quadril_esquerdo.setOnClickListener {
            openMenuApplication()
        }
    }

    private fun openMenuMain(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openMenuApplication(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
        finish()
    }

    private fun setPossibleArea(areas: MutableList<String>){
        val area_lis = listOf("6A", "6B")

        for (local in area_lis) {
            if (areas.contains(local) == false){
                when(local){
                    "6A" -> bt_a6_aa.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "6B" -> bt_a6_ab.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                }
            }
        }
    }

    fun consultToAppli(){
        var dataBase: FirebaseFirestore? = null
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.addSnapshotListener { local_data, error ->
            var data_his = local_data?.data

            val site_noappli: MutableList<String> = data_his?.get("list_not_appli") as MutableList<String>

            override@
            dispoArea = GetHistoricDB("").localAppli("6", site_noappli)

            setPossibleArea(dispoArea)
        }
    }

    fun getIdUser(): String {
        val usuarioAtual = Firebase.auth.currentUser
        val id = usuarioAtual?.uid
        return id.toString()
    }
}