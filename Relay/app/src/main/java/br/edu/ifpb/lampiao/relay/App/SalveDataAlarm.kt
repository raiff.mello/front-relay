package br.edu.ifpb.lampiao.relay.Fragments

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.Receiver.SampleBootReceiver
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*

class SalveDataAlarm {

    var context:Context?=null
    constructor(context:Context){
        this.context=context
    }

    private val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val tabela_aplicacao = db.collection("schedule").document(Id_usuarioAtual.toString())


    var alrmMan:AlarmManager? = null
    var alarmIntent: PendingIntent? = null
    val calendar: Calendar = Calendar.getInstance()



    fun ativarAlarmes(){
        alarmIntent = Intent(context, SampleBootReceiver::class.java).let { intent ->
            intent.action="SalveDataAlarm"
            PendingIntent.getBroadcast(context, 0, intent, 0)
        }
        alrmMan= context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        tabela_aplicacao.get()
            .addOnCompleteListener {
                if(it.isSuccessful) {
                    val userData = it?.result?.data
                    var horas = userData?.get("horas_aplicacao")
                    var minutos = userData?.get("minutos_aplicacao")

                    if ((horas != null && minutos != null)) {
                        val diasBd = userData?.get("dias_semanais_aplication") as ArrayList<Int>

                        calendar.apply {
                            timeInMillis = System.currentTimeMillis()
                            set(Calendar.HOUR_OF_DAY, horas.toString().toInt())
                            set(Calendar.MINUTE, minutos.toString().toInt())
                            set(Calendar.SECOND, 0)
                            set(Calendar.MILLISECOND, 0)
                        }
                        //edita o alarme para o dia exato, em vez de fazer a verificação todos os dias
                        //enquanto o o dia da semana registrado no calendar(hoje), não estiver dentro do array 'diasBd' ou se o alarme estiver no passado
                        while ((!(checkDay(calendar.get(Calendar.DAY_OF_WEEK), diasBd))) || (System.currentTimeMillis() > calendar.timeInMillis)) {
                            calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1)// somar 1 dia, até chegar em dia da semana que seja igual ao do 'diasBd'
                        }

                        alrmMan?.setExact(
                            AlarmManager.RTC_WAKEUP,
                            calendar.timeInMillis,
                            alarmIntent
                        )


                    }
                }
            }.addOnFailureListener {
                Toast.makeText(context, "Crie um alarme", Toast.LENGTH_SHORT).show()
            }

    }


    fun cancelAlarm(){
        alarmIntent = Intent(context, SampleBootReceiver::class.java).let { intent ->
            intent.action="SalveDataAlarm"
            PendingIntent.getBroadcast(context, 0, intent, 0)
        }
        alrmMan= context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (alarmIntent != null && alrmMan != null) {
            alrmMan?.cancel(alarmIntent)
        }

    }

    fun cancelDeleteAlarm(){
            cancelAlarm()
            tabela_aplicacao.delete().addOnSuccessListener {
                Toast.makeText(context, "Alarme Excluido", Toast.LENGTH_SHORT).show()

            }
            Toast.makeText(context, "Alarme Cancelado", Toast.LENGTH_SHORT).show()



    }

    fun checkDay(dia: Int, dias: ArrayList<Int>): Boolean {
        var dt = false
        for (n: Int in dias) {
            if (n == dia) {
                dt = true
            }
        }
        return dt
    }


}