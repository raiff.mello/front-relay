package br.edu.ifpb.lampiao.relay.Util

import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.ZoneData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class GetHistoricDB(month_date: String?) : AppCompatActivity(){
    private val _month_dt = month_date


/*------------------------------------Date cut functions------------------------------------------*/


    private fun getMonth(date: MutableList<String>?): MutableList<String>{
        if (date != null) {
            date.remove("")
        }
        val _month: MutableList<String> = mutableListOf()
        if (date != null) {
            for( mes: String in date){
                _month.add(mes.subSequence(3,5) as String)
            }
        }
        return _month
    }


    private fun getDay(date: MutableList<String>?): MutableList<String>{
        if (date != null) {
            date.remove("")
        }
        val _day: MutableList<String> = mutableListOf()
        if (date != null) {
            for( mes: String in date){
                _day.add(mes.subSequence(0,2) as String)
            }
        }

        return _day
    }


    private fun getTime(date: MutableList<String>?): MutableList<String>{
        if (date != null) {
            date.remove("")
        }
        val _time: MutableList<String> = mutableListOf()
        if (date != null) {
            for( mes: String in date){
                _time.add(mes.subSequence(11,16) as String)
            }
        }

        return _time
    }


    private fun getDate(date: MutableList<String>?): MutableList<String>{
        if (date != null) {
            date.remove("")
        }
        val _date: MutableList<String> = mutableListOf()
        if (date != null) {
            for( mes: String in date){
                _date.add(mes.subSequence(0,10) as String)
            }
        }
        return _date
    }




    /*---------------------------------Returning Functions----------------------------------------*/



    fun completeListTime(dateHis: MutableList<String>?): MutableList<String>{
        val num_month = numMonth(_month_dt)
        var list_time: MutableList<String> = listsPre()

        val month: MutableList<String> = getMonth(dateHis)
        val day: MutableList<String>   = getDay(dateHis)
        val time: MutableList<String>  = getTime(dateHis)

        var i = 0
        for (mes in month){
            if (mes == num_month){
                val daynum = numDay(day[i])
                list_time.set(daynum, time[i])
            }
            i++
        }
        return list_time
    }

    fun completeListSite(dateHis: MutableList<String>?, siteHis: MutableList<String>): MutableList<String>{
        val num_month = numMonth(_month_dt)
        var list_site: MutableList<String> = listsPre()

        val month: MutableList<String> = getMonth(dateHis)
        val day: MutableList<String>   = getDay(dateHis)

        var i = 0
        for (mes in month){
            if (mes == num_month){
                val daynum = numDay(day[i])
                list_site.set(daynum, siteHis[i+1])
            }
            i++
        }
        return list_site
    }

    fun completeListDate(dateHis: MutableList<String>?, siteHis: MutableList<String>): MutableList<String>{
        var list_date: MutableList<String> = listDatePre()
        val date: MutableList<String> = getDate(dateHis)

        var i = 1
        for (mes in date){
            val sitenum = numSite(siteHis[i])
            list_date.set(sitenum, mes)
            i++
        }
        return list_date
    }



    fun returnValuesSite(siteHis: MutableList<String>): MutableList<Int>{
        var list_values_site: MutableList<Int> = listValuesPre()

        for (site in siteHis){
            if (site != "") {
                val sitenum = numSite(site)
                var pos =  (siteHis.count() - siteHis.indexOf(site) )
                val posi = ((pos*3.33).toInt())+1
                list_values_site.set(sitenum, posi)
            }
        }
        return list_values_site
    }

    fun localAppli(area: String, lis: MutableList<String>): MutableList<String> {
        var lista_dispo = mutableListOf<String>()
        val list_pre = lis

        for (i in list_pre){
            if (i.subSequence(0,1) == area){
                lista_dispo.add(i)
            }
        }

        return lista_dispo
    }


    /*-------------------------------Filled Array Return Functions---------------------------------*/




    /*Function returns a predefined mutable list filled with "-" to be used in general history functions.*/
    private fun listsPre(): MutableList<String> {
        val list: MutableList<String> = mutableListOf()
        val days_month = numDayMonth(_month_dt)
        var i = 0
        while (i < 30){ //inserir days_month no lugar de 30
            list.add("-")
            i ++
        }
        return list
    }


    /*  Function returns mutable list to list of specific history dates.
        There are 30 values filled with empty data in the format "--/--/----". */
    private fun listDatePre(): MutableList<String> {
        val list: MutableList<String> = mutableListOf()
        var i = 0
        while (i < 30){
            list.add("--/--/----")
            i ++
        }
        return list
    }


    /*  Function returns a mutable list to list progress values for each area of the specific history.
        There are 30 values set at 30 to fill in the existing values. */
    private fun listValuesPre(): MutableList<Int> {
        val list: MutableList<Int> = mutableListOf()
        var i = 0
        while (i < 30){
            list.add(100)
            i ++
        }
        return list
    }



    /*----------------------------------Key Exchange Functions------------------------------------*/

    // Function returns the number of each month as a string.
    fun numMonth(month:String?): String{
        val _month = month?.toUpperCase()
        var num_month: String = ""
        when(_month){
            "JANEIRO" -> num_month = "01"
            "FEVEREIRO" -> num_month = "02"
            "MARÇO" -> num_month = "03"
            "ABRIL" -> num_month = "04"
            "MAIO" -> num_month = "05"
            "JUNHO" -> num_month = "06"
            "JULHO" -> num_month = "07"
            "AGOSTO" -> num_month = "08"
            "SETEMBRO" -> num_month = "09"
            "OUTUBRO" -> num_month = "10"
            "NOVEMBRO" -> num_month = "11"
            "DEZEMBRO" -> num_month = "12"
        }
        return num_month
    }
    fun numDayMonth(month:String?): Int{
        var num_month = 0
        when(month){
            "JANEIRO" -> num_month = 31
            "FEVEREIRO" -> num_month = 30
            "MARÇO" -> num_month = 31
            "ABRIL" -> num_month = 30
            "MAIO" -> num_month = 31
            "JUNHO" -> num_month = 30
            "JULHO" -> num_month = 31
            "AGOSTO" -> num_month = 31
            "SETEMBRO" -> num_month = 30
            "OUTUBRO" -> num_month = 31
            "NOVEMBRO" -> num_month = 30
            "DEZEMBRO" -> num_month = 31
        }
        return num_month
    }


    // Function returns position of each day for comparing information present in general history.
    fun numDay(day:String?): Int{
        var num_day = 0
        when(day){
            "01" -> num_day = 0
            "02" -> num_day = 1
            "03" -> num_day = 2
            "04" -> num_day = 3
            "05" -> num_day = 4
            "06" -> num_day = 5
            "07" -> num_day = 6
            "08" -> num_day = 7
            "09" -> num_day = 8
            "10" -> num_day = 9
            "11" -> num_day = 10
            "12" -> num_day = 11
            "13" -> num_day = 12
            "14" -> num_day = 13
            "15" -> num_day = 14
            "16" -> num_day = 15
            "17" -> num_day = 16
            "18" -> num_day = 17
            "19" -> num_day = 18
            "20" -> num_day = 19
            "21" -> num_day = 20
            "22" -> num_day = 21
            "23" -> num_day = 22
            "24" -> num_day = 23
            "25" -> num_day = 24
            "26" -> num_day = 25
            "27" -> num_day = 26
            "28" -> num_day = 27
            "29" -> num_day = 28
            "30" -> num_day = 29
            "31" -> num_day = 30
        }
        return num_day
    }


    // Function returns position of each area for specific history value list.
    fun numSite(day:String?): Int{
        var num_site = 0
        when(day){
            "1A" -> num_site = 0
            "1B" -> num_site = 1
            "1C" -> num_site = 2
            "1D" -> num_site = 3
            "1E" -> num_site = 4
            "1F" -> num_site = 5
            "1G" -> num_site = 6
            "1H" -> num_site = 7
            "1I" -> num_site = 8
            "1J" -> num_site = 9
            "2A" -> num_site = 10
            "2B" -> num_site = 11
            "2C" -> num_site = 12
            "2D" -> num_site = 13
            "2E" -> num_site = 14
            "2F" -> num_site = 15
            "3A" -> num_site = 16
            "3B" -> num_site = 17
            "3C" -> num_site = 18
            "3D" -> num_site = 19
            "3E" -> num_site = 20
            "3F" -> num_site = 21
            "4A" -> num_site = 22
            "4B" -> num_site = 23
            "5A" -> num_site = 24
            "5B" -> num_site = 25
            "6A" -> num_site = 26
            "6B" -> num_site = 27
            "7A" -> num_site = 28
            "7B" -> num_site = 29

        }
        return num_site
    }



}