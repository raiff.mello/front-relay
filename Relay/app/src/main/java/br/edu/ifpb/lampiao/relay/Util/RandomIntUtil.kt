package br.edu.ifpb.lampiao.relay.Util

import java.util.concurrent.atomic.AtomicInteger

object RandomIntUtil {
    private val seed = AtomicInteger()

    fun getRandomInt() = seed.getAndIncrement()+ System.currentTimeMillis().toInt()

    fun getRandomInt(min: Int, max: Int) = (min..max).random()
}