package br.edu.ifpb.lampiao.relay.Receiver

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import br.edu.ifpb.lampiao.relay.R

class AlertReceiver: BroadcastReceiver() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context?, intent: Intent?) {
        val notifyID = 1
        val CHANNEL_ID = "my_channel_01" // The id of the channel.

        val name: CharSequence =
            "Alarme" // The user-visible name of the channel.
        var string: String? = intent?.getStringExtra("dias_semana")

        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
// Create a notification and set the notification channel.
// Create a notification and set the notification channel.
        val notification: Notification = Notification.Builder(context)
            .setContentTitle(intent?.getStringExtra("mensagem"))
            .setContentText(intent?.getStringExtra("titulo")+"\n"+string)
            .setSmallIcon(R.drawable.ic_active_notification)
            .setChannelId(CHANNEL_ID)
            .build()

        val mNotificationManager = context?.applicationContext?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.createNotificationChannel(mChannel)

// Issue the notification.

// Issue the notification.
        mNotificationManager.notify(notifyID, notification)

    }
}