package br.edu.ifpb.lampiao.relay.Aplicacao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import br.edu.ifpb.lampiao.relay.Util.SaveApplicationDB
import br.edu.ifpb.lampiao.relay.Util.scheduleDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_aplicacao_barriga.*
import kotlinx.android.synthetic.main.activity_aplicacao_braco_direito.*
import kotlinx.android.synthetic.main.activity_aplicacao_quadril_lado_direito.*

class AplicacaoQuadrilLadoDireito : AppCompatActivity() {
    private var dispoArea = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao_quadril_lado_direito)

        supportActionBar?.hide()

        val typeface = ResourcesCompat.getFont(this, R.font.mordern_sans)
        bt_aplicar_tela_aplicacao_quadril_direito.typeface = typeface
        bt_cancelar_tela_aplicacao_quadril_direito.typeface = typeface

        consultToAppli()
        var siteApplication = ""

        bt_a7_aa.setOnClickListener {
            if (dispoArea.contains("7A")) {
                bt_a7_aa.isChecked = true
                bt_a7_ab.isEnabled = false
                siteApplication = "7A"
            }else {
                bt_a7_aa.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }

        }

        bt_a7_ab.setOnClickListener {
            if (dispoArea.contains("7B")) {
                bt_a7_ab.isChecked = true
                bt_a7_aa.isChecked = false
                siteApplication = "7B"
            }else {
                bt_a7_ab.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }

        }

        bt_aplicar_tela_aplicacao_quadril_direito.setOnClickListener {
            if (siteApplication != "") {
                SaveApplicationDB(siteApplication).saveDataDB()
                val schedule = scheduleDB(applicationContext)
                schedule.decrementAplication()
                openMenuMain()
            }else{
                Toast.makeText(this, "Você não selecionou uma área", Toast.LENGTH_SHORT).show()
            }
        }

        bt_cancelar_tela_aplicacao_quadril_direito.setOnClickListener {
            openMenuApplication()
        }
    }

    private fun openMenuMain(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openMenuApplication(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
        finish()
    }

    private fun setPossibleArea(areas: MutableList<String>){
        val area_lis = listOf("7A", "7B")

        for (local in area_lis) {
            if (areas.contains(local) == false){
                when(local){
                    "7A" -> bt_a7_aa.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "7B" -> bt_a7_ab.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                }
            }
        }
    }

    fun consultToAppli(){
        var dataBase: FirebaseFirestore? = null
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.addSnapshotListener { local_data, error ->
            var data_his = local_data?.data

            val site_noappli: MutableList<String> = data_his?.get("list_not_appli") as MutableList<String>

            override@
            dispoArea = GetHistoricDB("").localAppli("7", site_noappli)

            setPossibleArea(dispoArea)
        }
    }

    fun getIdUser(): String {
        val usuarioAtual = Firebase.auth.currentUser
        val id = usuarioAtual?.uid
        return id.toString()
    }
}