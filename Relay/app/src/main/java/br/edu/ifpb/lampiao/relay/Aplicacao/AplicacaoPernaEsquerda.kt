package br.edu.ifpb.lampiao.relay.Aplicacao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import br.edu.ifpb.lampiao.relay.App.Aplicacao
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import br.edu.ifpb.lampiao.relay.Util.SaveApplicationDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_aplicacao_barriga.*
import br.edu.ifpb.lampiao.relay.Util.scheduleDB
import kotlinx.android.synthetic.main.activity_aplicacao_perna_direita.*
import kotlinx.android.synthetic.main.activity_aplicacao_perna_esquerda.*

class AplicacaoPernaEsquerda : AppCompatActivity() {
    private var dispoArea = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aplicacao_perna_esquerda)

        supportActionBar?.hide()

        val typeface = ResourcesCompat.getFont(this, R.font.mordern_sans)
        bt_aplicar_tela_aplicacao_perna_esquerda.typeface = typeface
        bt_cancelar_tela_aplicacao_perna_esquerda.typeface = typeface

        consultToAppli()
        var siteApplication = ""

        bt_a3_aa.setOnClickListener {
            if (dispoArea.contains("3A")) {
                setButtonState()
                bt_a3_aa.isChecked = true
                siteApplication = "3A"
            }else {
                bt_a3_aa.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a3_ab.setOnClickListener {
            if (dispoArea.contains("3B")) {
                setButtonState()
                bt_a3_ab.isChecked = true
                siteApplication = "3B"
            }else {
                bt_a3_ab.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a3_ac.setOnClickListener {
            if (dispoArea.contains("3C")) {
                setButtonState()
                bt_a3_ac.isChecked = true
                siteApplication = "3C"
            }else {
                bt_a3_ac.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a3_ad.setOnClickListener {
            if (dispoArea.contains("3D")) {
                setButtonState()
                bt_a3_ad.isChecked = true
                siteApplication = "3D"
            }else {
                bt_a3_ad.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a3_ae.setOnClickListener {
            if (dispoArea.contains("3E")) {
                setButtonState()
                bt_a3_ae.isChecked = true
                siteApplication = "3E"
            }else {
                bt_a3_ae.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_a3_af.setOnClickListener {
            if (dispoArea.contains("3F")) {
                setButtonState()
                bt_a3_af.isChecked = true
                siteApplication = "3F"
            }else {
                bt_a3_af.isEnabled = false
                Toast.makeText(this, "Você não pode aplicar " +
                        "nesse local! Escolha outro.", Toast.LENGTH_SHORT).show()
            }
        }

        bt_aplicar_tela_aplicacao_perna_esquerda.setOnClickListener {
            if (siteApplication != "") {
                SaveApplicationDB(siteApplication).saveDataDB()
                val schedule = scheduleDB(applicationContext)
                schedule.decrementAplication()
                openMenuMain()
            }else{
                Toast.makeText(this, "Você não selecionou uma área", Toast.LENGTH_SHORT).show()
            }
        }

        bt_cancelar_tela_aplicacao_perna_esquerda.setOnClickListener {
            openMenuApplication()
        }
    }

    private fun setButtonState(){
        bt_a3_aa.isChecked = false
        bt_a3_ab.isChecked = false
        bt_a3_ac.isChecked = false
        bt_a3_ad.isChecked = false
        bt_a3_ae.isChecked = false
        bt_a3_af.isChecked = false
    }
    private fun setPossibleArea(areas: MutableList<String>){
        val area_lis = listOf("3A", "3B", "3C", "3D", "3E", "3F")

        for (local in area_lis) {
            if (areas.contains(local) == false){
                when(local){
                    "3A" -> bt_a3_aa.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "3B" -> bt_a3_ab.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "3C" -> bt_a3_ac.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "3D" -> bt_a3_ad.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "3E" -> bt_a3_ae.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                    "3F" -> bt_a3_af.setButtonDrawable(R.drawable.unavailable_checkbox_30dp)
                }
            }
        }
    }

    fun consultToAppli(){
        var dataBase: FirebaseFirestore? = null
        dataBase = FirebaseFirestore.getInstance()

        val reference = dataBase.collection("usuarios").document(getIdUser())
                .collection("historic").document("historic")

        reference.addSnapshotListener { local_data, error ->
            val data_his = local_data?.data

            val site_noappli: MutableList<String> = data_his?.get("list_not_appli") as MutableList<String>

            override@
            dispoArea = GetHistoricDB("").localAppli("3", site_noappli)

            setPossibleArea(dispoArea)
        }
    }

    fun getIdUser(): String {
        val usuarioAtual = Firebase.auth.currentUser
        val id = usuarioAtual?.uid
        return id.toString()
    }

    private fun openMenuMain(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openMenuApplication(){
        val intent = Intent(this, Aplicacao::class.java)
        startActivity(intent)
        finish()
    }
}