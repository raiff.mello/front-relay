package br.edu.ifpb.lampiao.relay.RecyclerView

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.ifpb.lampiao.relay.HorizontalSpinner.Adapter.ZoneAdapter
import br.edu.ifpb.lampiao.relay.R
import br.edu.ifpb.lampiao.relay.RecyclerView.Data.BodyData
import br.edu.ifpb.lampiao.relay.RecyclerView.Model.ZoneViewModel
import br.edu.ifpb.lampiao.relay.Util.EnumBodyFields
import br.edu.ifpb.lampiao.relay.Util.GetHistoricDB
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_zone.*

class ZoneFragment: Fragment() {
    private lateinit var zoneViewModel: ZoneViewModel

    private lateinit var zoneAdapter: ZoneAdapter

    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var bodyPartSelected: EnumBodyFields
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_zone, container, false)

        layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        return view
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        zoneViewModel = ViewModelProvider(this).get(ZoneViewModel::class.java)

        zoneAdapter = ZoneAdapter(arrayOf(), context)

        zone_list.adapter = zoneAdapter
        zone_list.layoutManager = layoutManager
    }

    fun setBodyPartSelectd(bodyPart: BodyData?) {
        bodyPartSelected = bodyPart!!.bodyPartEnum
        if(zoneViewModel.zones != null) {
            zoneAdapter.update(zoneViewModel.zones!![bodyPartSelected.ordinal])
        }
    }
}