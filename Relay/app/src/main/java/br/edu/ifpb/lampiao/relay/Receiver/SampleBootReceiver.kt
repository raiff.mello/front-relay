package br.edu.ifpb.lampiao.relay.Receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast
import br.edu.ifpb.lampiao.relay.Fragments.NotificationAlarm
import br.edu.ifpb.lampiao.relay.Fragments.SalveDataAlarm
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*
import kotlin.collections.ArrayList


class SampleBootReceiver : BroadcastReceiver() {
    val db = Firebase.firestore
    val usuarioAtual = Firebase.auth.currentUser
    val Id_usuarioAtual = usuarioAtual?.uid
    val tabela_aplicacao = db.collection("schedule").document(Id_usuarioAtual.toString())
    val calendar: Calendar = Calendar.getInstance()
    var diaHj = calendar.get(Calendar.DAY_OF_WEEK)


    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            val saveData = SalveDataAlarm(context)
            saveData.ativarAlarmes()
        } else if (intent.action == "SalveDataAlarm") {

            tabela_aplicacao.get().addOnCompleteListener {
                if (it.isSuccessful) {
                    val userData = it?.result?.data
                    val diasBd = userData?.get("dias_semanais_aplication") as ArrayList<Int>
                    if (checkDay(diaHj, diasBd)){

                        val saveData = SalveDataAlarm(context)
                        saveData.ativarAlarmes() // ativar o proximo
                        startAlarmService(context, intent)

                    }
                }

            }
        }else if (intent.action == "stopAlarm") {
            val intentService = Intent(context, NotificationAlarm::class.java)
            context.stopService(intentService)
        }

    }

    private fun startAlarmService(context: Context, intent: Intent) {
        val intentService = Intent(context, NotificationAlarm::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intentService)
        } else {
            context.startService(intentService)
        }
    }







    private fun checkDay(dia: Int, dias: ArrayList<Int>): Boolean {
        var dt = false
        for (n: Int in dias) {
            if (n == dia) {
                dt = true
            }
        }
        return dt
    }



}



