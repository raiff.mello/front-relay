package br.edu.ifpb.lampiao.relay.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpb.lampiao.relay.App.About
import br.edu.ifpb.lampiao.relay.Fragments.SalveDataAlarm
import br.edu.ifpb.lampiao.relay.MainActivity
import br.edu.ifpb.lampiao.relay.R
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import kotlinx.android.synthetic.main.activity_login.*


class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        verifyLoggedUser()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar!!.hide()


        bt_login.setOnClickListener {
            authUser()
        }

        register.setOnClickListener {
            openRegister()
        }

        forgot_password.setOnClickListener {
            openForgotPassword()
        }

        bt_about.setOnClickListener {
            goToAbout()
        }
    }

    fun authUser() {
        val email = email_field_login.text.toString()
        val pass = pass_field_login.text.toString()
        if (email.isEmpty() || pass.isEmpty()) {
            error_message_login.text = "Coloque o seu e-mail e senha para fazer o login!"
        } else {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            error_message_login.text = "Login realizado com sucesso!"
                            openMainMenu()
                            val saveData = SalveDataAlarm(applicationContext)
                            saveData.ativarAlarmes()
                        }
                    }.addOnFailureListener {
                        when (it) {
                            is FirebaseAuthInvalidCredentialsException -> error_message_login.text = "Email ou senha incorretos! Por favor, verifique."
                            is FirebaseNetworkException -> error_message_login.text = "Erro de conexão! Verifique se seu dispositivo está conectado a uma rede de Internet."
                            else -> error_message_login.text = "Erro ao logar!"
                        }
                    }
        }
    }

    private fun verifyLoggedUser() {
        val actualuser = FirebaseAuth.getInstance().currentUser
        if (actualuser != null) {
            openMainMenu()
            finish()
        }
    }

    private fun openRegister() {
        val intent = Intent(this, Cadastro::class.java)
        startActivity(intent)
    }

    private fun openMainMenu() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun openForgotPassword(){
        val intent = Intent(this, ForgotPassword::class.java)
        startActivity(intent)
    }

    private fun goToAbout(){
        val intent = Intent(this, About::class.java)
        startActivity(intent)

    }



}