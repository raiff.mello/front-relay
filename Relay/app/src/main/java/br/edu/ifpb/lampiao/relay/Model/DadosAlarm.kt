package br.edu.ifpb.lampiao.relay.Model

data class DadosAlarm(
    val dias_semanais_aplication: ArrayList<Int> = arrayListOf<Int>(),
    val horas_aplicacao:Int? = null,
    val minutos_aplicacao:Int? = null,
    val quantidade:Int? = null

)